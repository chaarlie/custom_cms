<?php 

include('classes/ZerosDesign.php');
include('includes/header.php');
include('classes/DatabaseOperations2.php');

$dbOp = new DatabaseOperations2();

?>

<body id="blog">
<div id="page">



<header id="header">
	<div class="header_inner wrapper">
	
        <div class="header_top clearfix">
            <div id="logo" class="left_float">
                <a class="logotype" href="index.php"><img src="resources/images/logo.png" alt="Logotype" width="80" height="50"></a>  
            </div>
            
            <nav id="nav" class="right_float">
                <ul>
                    <li><a href="index.php">Home</a></li>
              		<li><a href="about.php">Nosotros</a></li>                   
                    <li><a href="portfolio.php" >Folio</a>
          				<ul>
                            <li><a href="portfolio.php?columns=4">4 Columnas</a></li>
                        	<li><a href="portfolio.php?columns=3" class="active">3 Columnas</a></li>
                        	<li><a href="portfolio.php?columns=2">2 Columnas</a></li>                            
                      </ul>
                    </li>
              		<li><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contacto</a></li>
                </ul>
            </nav>
            
        </div>

         <div class="header_tagline seperator-section">
            <h1><strong>Business Branding</strong></h1>
             <h3></h3> 
        </div>
       
    </div>	
	<script type="text/javascript">

	$(function(){
	    $("div.meta_likes").click(function(event){
	        event.preventDefault();
	        var id = $(this).attr("value");

	        $.ajax({
	            method:'get',
	            url:'increment_articles.php',
	            data:{id:id,likes:true},            
	            success:function(){
	                var new_like = parseInt($("#meta_likes_"+ id + " a").html());
	                new_like++;
	                //un workaround: aparentemente jQuery no me retorna el valor
	                document.getElementById("meta_likes_"+id).childNodes.item(0).innerHTML = new_like;     
	            }
	        });
	    });
	})

	</script>
</header>


<section id="pagetitle">
	<div class="pagetitle_inner wrapperoverlay">
		<h2><strong>Blog</strong><!-- <span class="tagline">Noticias </span> --></h2>
    </div>
</section>


<section id="main">
    <div class="main_inner wrapper clearfix">        
        <article id="maincontent" class="left_float">
			
			<?php

			$meses = array(
						"Enero", "Febrero", "Marzo",
						"Abril", "Mayo", "Junio", "Julio",
						"Agosto", "Septiembre", "Octubre",
						"Noviembre", "Diciembre"
					);

			$next = 0;
			$previous = 0;			
			$rows = array();

			if (isset($_GET['next'])) {
				$next = intval($_GET['next']);

				$first_article_id = 0;			
				$last_row_id = 0;
				$result = $dbOp->select('', 'articles', "SELECT articles_id FROM articles LIMIT 1");			
				$first_article_id = $next != 0? $next : $result[0][0];			
				$rows = $dbOp->select('' , 'articles', "SELECT * FROM articles WHERE articles_id >= $first_article_id LIMIT 3 ");			
				
			}

			elseif (isset($_GET['previous'])) {
				$previous = intval($_GET['previous']);
				$last_article_id = 0;			
				$last_row_id = 0;
				$result = $dbOp->select('', 'articles', "SELECT articles_id FROM articles ORDER BY articles_id DESC LIMIT 1");			
				$last_article_id = $previous != 0? $previous : $result[0][0];			
				$rows = $dbOp->select('', 'articles', "SELECT * FROM articles WHERE articles_id <= $last_article_id ORDER BY articles_id DESC LIMIT 3 ");			
				
			}
			else{
				$rows = $dbOp->select('', 'articles', "SELECT * FROM articles  LIMIT 3 ");	
				

			}

			$last_article_id = 0;
			foreach ($rows as $key){

				$last_article_id = $article_id = $key[0];
				$article_categories = $dbOp->select('', '', "SELECT DISTINCT category_name  FROM articles_blog_categories NATURAL JOIN blog_categories WHERE articles_blog_categories.articles_id = $article_id");
				$total_categories = array();
				
				if(isset($article_categories[0])){
					foreach ($article_categories as $cat) 
						$total_categories[] = $cat[0];					
				}
				$total_categories = implode(', ', $total_categories);

			?>
			

        	
        	<?php

            $article_imgs = $dbOp->select('', '', "SELECT img_src FROM article_imgs NATURAL JOIN articles WHERE article_imgs.articles_id = $article_id"); 
								
                if (count($article_imgs) > 0){                          

        	?>
            	<div class="entry clearfix" >
             	<div class="entry-meta left_float clearfix">
                <div class="meta_type"><a href="" class="type_gallery"></a></div>                
                <div class="meta_date"><b><?php echo  strftime("%B %d, %Y",(strtotime($key[2]))) ?></b></div>                    
                <div class="meta_tags"><?php echo $key[3] ?></div>                               
                <div class="meta_views "><?php echo $key[6] ?></div>
                <div class="meta_likes" value="<?php echo $key[0] ?>" id="meta_likes_<?php echo $key[0] ?>"><a href=""><?php echo $key[7] ?></a></div>                    
                <div class="meta_tags"><?php echo "$total_categories" ?></div>
            </div>

                <div class="entry-content right_float" >
					<div class="entry-thumb">
					                   									
                        <div id="slider" class="slidercontent">        
                            <div class="flexslider">
                                <ul class="slides">
                                  <?php  

                                  foreach ($article_imgs as $img){
                                                                                                                                                                                          
                                  ?>
                                    <li>
                                        <img src="resources/images/blog/<?php  echo $img[0] ?>"/>
                                    </li>
                                  <?php

                                  }

                                  ?>
                                </ul>
                            </div>
                        </div>


                     <div class="entry-info">
						<div class="post-headline">
							<h3><strong><a href="blog_single.php?id=<?php  echo $article_id ?>"><?php  echo $key[5] ?></a></strong></h3>
						</div>
						<p><?php  echo $key[4] ?></p>
					</div>		
                    </div>
                </div>
            </div>
                <?php

                }

                ?>
				<?php

                $article_vimeo = $dbOp->select('', '', "SELECT content,creation ,category,description,title,url FROM article_links NATURAL JOIN articles WHERE article_links.articles_id = $article_id LIMIT 1");     
                
                if (isset($article_vimeo[0][0])) {
            	
            	?>

                	<div class="entry clearfix" >
                	<div class="entry-meta left_float clearfix">
                    <div class="meta_type"><a href="" class="type_video"></a></div>
                    <!-- <div class="meta_date"><b><?php echo  date (" d M Y",strtotime($key[2])) ?></b></div>                     -->
                    <div class="meta_date"><b><?php echo  strftime("%B %d, %Y",(strtotime($key[2]))) ?></b></div>                    
                    <div class="meta_tags"><?php echo $key[3] ?></div>                                   
                    <div class="meta_views "><?php echo $key[6] ?></div>
                    <div class="meta_likes" value="<?php echo $key[0] ?>" id="meta_likes_<?php echo $key[0] ?>"><a href=""><?php echo $key[7] ?></a></div>                    
                    <div class="meta_tags"><?php echo "$total_categories" ?></div>
                </div>
 
                           
                <div class="entry-content right_float" >

                	<?php    	
                    
                    foreach ($article_vimeo as $vimeo) {                    
					
					?>
					
					<div class="entry-thumb">
					<?php if(strlen($vimeo[5]) > 1 ){?>
                    	<div class="embeddedvideo">
                    		<?php  echo str_replace("\\", "", $vimeo[5]) ?>
                        </div>
                    <?php

                	}

                    ?>
		            </div>
		                    
                     <div class="entry-info">
						<div class="post-headline">
							<h3><strong><a href="blog_single.php?id=<?php  echo $article_id ?>"><?php  echo $key[5] ?></a></strong></h3>
						</div>
						<p><?php  echo $key[4] ?></p>
					</div>		

		                
					<?php

					}

					?>
				</div>
			</div>
				<?php
				
				}
				
				?>
				<?php 

				$article_audiovisual = $dbOp->select('', '', "SELECT element_src,element_type FROM audiovisual_elements NATURAL JOIN articles WHERE audiovisual_elements.articles_id = $article_id LIMIT 1"); 

				if (count($article_audiovisual) > 0){
					$audio_formats = array('audio/ogg','audio/mpeg','audio/mp4','audio/ogg','audio/wav');

                	foreach ($article_audiovisual as $audiovisual){   

                    ?>

                    <div class="entry clearfix" >
                 	<div class="entry-meta left_float clearfix">
                    <div class="meta_type"><a href="" class="type_audio"></a></div>                    
                    <div class="meta_date"><b><?php echo  strftime("%B %d, %Y",(strtotime($key[2]))) ?></b></div>                    
                    <div class="meta_tags"><?php echo $key[3] ?></div>                                   
                    <div class="meta_views "><?php echo $key[6] ?></div>
                    <div class="meta_likes" value="<?php echo $key[0] ?>" id="meta_likes_<?php echo $key[0] ?>"><a href=""><?php echo $key[7] ?></a></div>                    
                    <div class="meta_tags"><?php echo "$total_categories" ?></div>
                </div>
 
                          
                <div class="entry-content right_float" >
                	<div class="entry-thumb">
                		<script type="text/javascript">
							$(document).ready(function(){
								if($().jPlayer) {
									$("#jquery_jplayer").jPlayer({
										ready: function () {
											$(this).jPlayer("setMedia", {											
												oga: "resources/audiovisuals/<?php echo $audiovisual[0] ?>",  
												end: ""
											});
										},									
										cssSelectorAncestor: "#jp_interface",
										supplied: "oga,mp3,  all"
									});
								
								}
							});
						</script>	
					
						<div id="jquery_jplayer" class="jp-jplayer jp-jplayer-audio"></div>
			
						<div class="jp-audio-container">
							<div class="jp-audio">
								<div class="jp-type-single">
									<div id="jp_interface" class="jp-interface">
										<ul class="jp-controls">
			
											<li><div class="seperator-first"></div></li>
											<li><div class="seperator-second"></div></li>
											<li><a href="#" class="jp-play" tabindex="1">play</a></li>
											<li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
											<li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
											<li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
										</ul>
			
										<div class="jp-progress-container">
											<div class="jp-progress">
												<div class="jp-seek-bar">
													<div class="jp-play-bar"></div>
												</div>
											</div>
										</div>
										<div class="jp-volume-bar-container">
											<div class="jp-volume-bar">
			
												<div class="jp-volume-bar-value"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
		
                    </div>


                     <div class="entry-info">
						<div class="post-headline">
							<h3><strong><a href="blog_single.php?id=<?php  echo $article_id ?>"><?php  echo $key[5] ?></a></strong></h3>
						</div>
						<p><?php  echo $key[4] ?></p>
					</div>		

                    </div>	                    
					</div>		
								
			<?php
					}
				}

			?>						

            
        	<?php

			}

			$previous_article = $last_article_id;	
			$next_article = $last_article_id;	
			$result = $dbOp->select('' , 'articles', "SELECT articles_id FROM articles ORDER BY articles_id DESC LIMIT 1 ");			
			$last_row_id = $result[0][0];
			$result = $dbOp->select('' , 'articles', "SELECT articles_id FROM articles ORDER BY articles_id ASC LIMIT 1 ");			
			$first_row_id = $result[0][0];
	
			if($next_article >= $last_row_id) 
				$next_article = 0;
			if($previous_article <= $first_row_id ) 
				$previous_article = $last_row_id;
		?>

	    <div id="custom_pagination" class="right_float">
	    	<a href="blog.php?previous=<?php echo $previous_article?>" class="nav-next">Publicaciones Anteriores </a>
	    	<a href="blog.php?next=<?php echo $next_article?>" class="nav-prev">Publicaciones Siguientes</a>
	    </div> 		
        
        </article>
        
        <aside id="sidebar" class="right_float">
        	<section class="sidebar_section seperator">
            	<div class="widget">
                    <h6 class="sectiontitle">CATEGORIAS DEL BLOG</h6>
                    <div id="menu-widget" >
                        <ul>
                        	<?php

                   			$categories = $dbOp->select('*', 'blog_categories', "");                         
                    		
                    		foreach ($categories as $category) {                    
							
							?>
                            <li><a href=""><?php echo $category[1] ?></a></li>
                    		<?php
                			
                			}
                    		
                    		?>
                        </ul>
                    </div>
                </div>
            </section>
            
            <section class="sidebar_section seperator">
            	<div class="widget">
                    
                    <div class="text-widget" >
                    	<div id="twitter-widget">
                        	<a class="twitter-timeline" href="https://twitter.com/ZerosDesignSrl" data-widget-id="401390100279332864">Tweets por @ZerosDesignSrl</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
						</div>

                    </div>

                </div>
            </section>
            
        </aside>
        
        
	</div> <!-- END #main_inner -->     
</section> <!-- END #main -->


          
<?php

include('includes/footer2.php');
?>