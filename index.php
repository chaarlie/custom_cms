<?php 

include('classes/ZerosDesign.php');
include('includes/header.php');
include('classes/DatabaseOperations2.php');

$dbOp = new DatabaseOperations2();

?>
<body id="home">
<div id="page">




<header id="header">
    <div class="header_inner wrapper">
    
        <div class="header_top clearfix">
            <div id="logo" class="left_float">
                <a class="logotype" href="index.php"><img src="resources/images/logo.png" alt="Logotype" width="80" height="50"></a>  
            </div>
            
            <nav id="nav" class="right_float">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">Nosotros</a></li>                   
                    <li><a href="portfolio.php" >Folio</a>
                        <ul>
                            <li><a href="portfolio.php?columns=4">4 Columnas</a></li>
                            <li><a href="portfolio.php?columns=3" class="active">3 Columnas</a></li>
                            <li><a href="portfolio.php?columns=2">2 Columnas</a></li>                            
                      </ul>
                    </li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contacto</a></li>
                </ul>
            </nav>
            
        </div>

         <div class="header_tagline seperator-section">
            <h1><strong>Business Branding</strong></h1>
             <h3></h3> 
        </div>
       
    </div>  
    
</header>

     
<div id="animationsection" class="clearfix">
    
    <div id="slidersection">
        <div id="slider" class="slidermain">        
            <div class="flexslider" style="">
                <ul class="slides">
                    
                    <?php

                    $result = $dbOp->select('*' , 'index_banners', '');
                    foreach ($result as $key ){  ?>
                        <li>
                            <?php if($key[3] != ''){ ?>
                                <img src="resources/images/<?php echo $key[2]; ?> " />
                                <div class="flex-caption">
                                    <h4><strong><?php echo $key[1]; ?></strong></h4>
                                    <p><?php echo $key[3]; ?></p>
                                </div>
                            <?php 
                            }//END if
                            else
                                if($key[3] == ''){
                            ?>
                                    <img src="resources/images/<?php echo $key[2]; ?> " />
                                <?php
                                }// END if
                                ?>
                        </li>
                    <?php
                    }// END foreach

                    ?>
                </ul>
            </div>
        </div>
    </div>
    
    <div id="loadingsection">
      <div id="pageloader" class="clearfix"> 
            <!-- CONTENT WILL BE LOADED HERE -->   
      </div>
    </div>
    
    <div id="loader"><div id="mainIndexDiv" class="wrapper"><div class="loadingicon"><span><i>Loading</i></span></div></div></div>
        
</div>    


<section id="main">
<div class="main_inner wrapper clearfix">
        
        <div id="recentworks" class="seperator">
        
            

            <ul class="filter">
                <li><a class="active" href="" data-option-value="*">Todos</a></li>
                <?php

                $result = $dbOp->select('*', 'portfolio_filters', '');
                foreach ($result as $key ) {

                ?>

                <li><a href="" data-option-value=".<?php echo $key[1]; ?>"><?php echo $key[1]; ?></a></li>
                <?php
                
                }

                ?>
            </ul>
            
            <div id="masonry" class="portfolio-entries columns4 clearfix">
                <?php

                $result = $dbOp->select('', '', 'SELECT portfolio_items_id,option_value,img_src,alt,title FROM portfolio_filters JOIN portfolio_items WHERE portfolio_items.portfolio_filters_id = portfolio_filters.portfolio_filters_id  GROUP BY portfolio_items_id' );
                foreach ($result as $key ){

                ?>
                <div class="masonry_item portfolio-entry post <?php echo $key[1]; ?>">

                    <div class="imgoverlay">                        
                        <a href="portfolio_single.php?id=<?php echo $key[0]; ?>"><img src="resources/images/portfolio/<?php echo $key[2]; ?>" alt="<?php echo $key[3]; ?>"/></a>
                    </div>
                    <div class="portfolio-meta">
                        <h5><a href="portfolio-single.html"><strong><?php echo $key[4]; ?></strong></a></h5>
                        <span class="portfolio-categories"><?php echo $key[1]; ?></span>
                    </div>
                </div>
                <?php

                }//END foreach

                ?>
                
            </div><!-- END #masonry --> 
        
        </div><!-- END #recentworks -->
        
        <div id="latestnews" class="seperator">
            <h6 class="sectiontitle">&Uacute;ltimas Noticias</h6>

            <?php

            $news_result = $dbOp->select('', 'articles', 'SELECT articles_id,title,creation,description FROM articles  ORDER BY articles_id ASC LIMIT 3');

            $last = 0;

            foreach ($news_result as $news){
                $article_id = $news[0];
                
                if($last == 0)
                    $last = count($news_result);
                        
                    $article_imgs = $dbOp->select('', '', "SELECT img_src FROM article_imgs WHERE articles_id = $article_id");                                     
                                                  
                    if (count($article_imgs) > 0){        
                        $last--;                                                                    
            ?>
                        <div class="column one_third <?php echo $last == 0? 'last':''; ?>">                                    
                            <div class="entry clearfix">
                                <div class="entry-thumb">
                                    <div id="slider" class="slidercontent">        
                                        <div class="flexslider">
                                            <ul class="slides">

                            <?php

                            foreach ($article_imgs as $img){

                            ?>
                                <li>
                                    <img src="resources/images/blog/<?php echo $img[0] ?>" alt="Blog Post 03"/>
                                </li>
                            <?php

                            }

                            ?>
                                            </ul>
                                        </div>          
                                    </div>
                                </div> 

                                <div class="entry-content clearfix">
                                    <div class="entry-meta left_float">
                                        <div class="meta_type"><a href="blog_single.php?id=<?php echo $article_id ?>" class="type_gallery"></a></div>
                                    </div>
                
                                    <div class="entry-info right_float">
                                        <div class="post-headline">
                                            <h4><strong><a href="blog_single.php?id=<?php echo $article_id ?>"><?php echo $news[1] ?></a></strong></h4>
                                            <span class="entry-date"><?php echo  strftime("%B %d, %Y", (strtotime($news[2]))) ?></span>
                                        </div>
                                        <p>
                                            <?php echo $news[3] ?>
                                        </p>
                       
                                    </div>
                                </div>

                            </div>
                         </div><!-- END .column one_third-->                                          
                        
                        <?php 

                        }

                        $article_video = $dbOp->select('','',"SELECT url FROM article_links WHERE articles_id = $article_id");

                        if(isset($article_video[0][0]))   {
                            $video = $article_video[0][0];                                                                                                                                                                      
                            $last--;
                        ?>

                        <div class="column one_third <?php echo $last == 0? 'last':''; ?>">
                            <div class="entry clearfix">
                                <div class="entry-thumb">
                                    <div class="embeddedvideo">
                                        <?php echo str_replace("\\", "", $video) ?>
                                    </div>
                  
                                </div> 

                                <div class="entry-content clearfix">
                                    <div class="entry-meta left_float">
                                        <div class="meta_type"><a href="blog_single.php?id=<?php echo $article_id ?>" class="type_video"></a></div>
                                    </div>
                
                                    <div class="entry-info right_float">
                                        <div class="post-headline">
                                            <h4><strong><a href="blog_single.php?id=<?php echo $article_id ?>"><?php echo $news[1] ?></a></strong></h4>
                                            <span class="entry-date"><?php echo  strftime("%B %d, %Y", (strtotime($news[2]))) ?></span>
                                        </div>
                                        <p>
                                            <?php echo $news[3] ?>
                                        </p>
                       
                                    </div>
                                </div>

                            </div>
                        </div><!-- END .column one_third-->

                        <?php                            
                        }

                        $audio_visual = $dbOp->select('','', "SELECT  element_src FROM audiovisual_elements WHERE articles_id = $article_id");                                     
                                                      
                        if (isset($audio_visual[0][0])){                                   
                            $element = $audio_visual[0][0];                                                                                
                            $last--;
                        ?>
                        <div class="column one_third <?php echo $last == 0? 'last':''; ?>">
                            <div class="entry clearfix">
                                <div class="entry-thumb">

                                    <script type="text/javascript">
                                        $(document).ready(function(){
                                            if($().jPlayer) {
                                                $("#jquery_jplayer").jPlayer({
                                                    ready: function () {
                                                        $(this).jPlayer("setMedia", {
                                                            mp3: "resources/audiovisuals/<?php echo $element ?>",
                                                            oga: "resources/audiovisuals/<?php echo $element ?>",
                                                            END: ""
                                                        });
                                                    },
                                                    swfPath: "files/jplayer",
                                                    cssSelectorAncestor: "#jp_interface",
                                                    supplied: "oga,mp3,  all"
                                                });
                                            
                                            }
                                        });
                                    </script>
                                
                                    <div id="jquery_jplayer" class="jp-jplayer jp-jplayer-audio"></div>
                        
                                    <div class="jp-audio-container">
                                        <div class="jp-audio">
                                            <div class="jp-type-single">
                                                <div id="jp_interface" class="jp-interface">
                                                    <ul class="jp-controls">
                        
                                                        <li><div class="seperator-first"></div></li>
                                                        <li><div class="seperator-second"></div></li>
                                                        <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                                                        <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                                                        <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                                                        <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                                                    </ul>
                        
                                                    <div class="jp-progress-container">
                                                        <div class="jp-progress">
                                                            <div class="jp-seek-bar">
                                                                <div class="jp-play-bar"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="jp-volume-bar-container">
                                                        <div class="jp-volume-bar">
                                                            <div class="jp-volume-bar-value"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                
                                </div> 

                                <div class="entry-content clearfix">
                                    <div class="entry-meta left_float">
                                        <div class="meta_type"><a href="blog_single.php?id=<?php echo $article_id ?>" class="type_audio"></a></div>
                                    </div>
                
                                    <div class="entry-info right_float">
                                        <div class="post-headline">
                                            <h4><strong><a href="blog_single.php?id=<?php echo $article_id ?>"><?php echo $news[1] ?></a></strong></h4>
                                            <span class="entry-date"><?php echo  strftime("%B %d, %Y",(strtotime($news[2]))) ?></span>
                                        </div>
                                        <p>
                                            <?php echo $news[3] ?>
                                        </p>
                       
                                    </div>
                                </div>

                            </div>
                        </div><!-- END .column one_third-->                                    
                    <?php                                    
                    }// END if                    
                }// END foreach
                ?>

        </div> <!-- END #latestnews -->        
    </div> <!-- END #main_inner -->     
</section> <!-- END #main -->

<?php
include('includes/footer2.php');
?>