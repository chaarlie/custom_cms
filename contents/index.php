     
<div id="animationsection" class="clearfix">
    
    <div id="slidersection">
        <div id="slider" class="slidermain">        
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <img src="resources/images/slider-01.jpg" />
                    </li>
                    <li>
                        <img src="resources/images/slider-02.jpg" />
                        <div class="flex-caption"><h4><strong>Clean &amp; Responsive</strong></h4><p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Duis mollis, est non</p></div>
                    </li>
                    <li>
                        <img src="resources/images/slider-03.jpg" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <div id="loadingsection">
      <div id="pageloader" class="clearfix"> 
            <!-- CONTENT WILL BE LOADED HERE -->   
      </div>
    </div>
    
    <div id="loader"><div class="wrapper"><div class="loadingicon"><span><i>Loading</i></span></div></div></div>
        
</div>    


<section id="main">
<div class="main_inner wrapper clearfix">
        
        <div id="recentworks" class="seperator">
        
            <ul class="filter">
                <li><a class="active" href="" data-option-value="*">All</a></li>
                <li><a href="" data-option-value=".motion">Motion</a></li>
                <li><a href="" data-option-value=".web">Web</a></li>
                <li><a href="" data-option-value=".print">Print</a></li>
            </ul>
            
            
            <div id="masonry" class="portfolio-entries columns4 clearfix">
            	<div class="masonry_item portfolio-entry motion">
                    <div class="imgoverlay">
                        <a href="portfolio-single.html" class="loadcontent" rel="nullam-id-dolor"><img src="resources/images/portfolio/thumb-work_01.jpg" alt="Work 01"/></a>
                    </div>
                    <div class="portfolio-meta">
                    	<h5><a href="portfolio-single.html" class="loadcontent" rel="nullam-id-dolor"><strong>Nullam id dolor</strong></a></h5>
                        <span class="portfolio-categories">Motion</span>
                    </div>
              	</div>
                
                <div class="masonry_item portfolio-entry print">
                    <div class="imgoverlay">
                        <a href="portfolio-single.html" class="loadcontent" rel="duo-dolores"><img src="resources/images/portfolio/thumb-work_02.jpg" alt="Work 02"/></a>
                    </div>
                    <div class="portfolio-meta">
                    	<h5><a href="portfolio-single.html" class="loadcontent" rel="duo-dolores"><strong>Duo dolores</strong></a></h5>
                        <span class="portfolio-categories">Print</span>
                    </div>
              	</div>
                
                <div class="masonry_item portfolio-entry print">
                    <div class="imgoverlay">
                        <a href="portfolio-single.html" class="loadcontent" rel="takimata-sanctus"><img src="resources/images/portfolio/thumb-work_03.jpg" alt="Work 03"/></a>
                    </div>
                    <div class="portfolio-meta">
                    	<h5><a href="portfolio-single.html" class="loadcontent" rel="takimata-sanctus"><strong>Takimata sanctus</strong></a></h5>
                        <span class="portfolio-categories">Print</span>
                    </div>
              	</div>
                
                <div class="masonry_item portfolio-entry web">
                    <div class="imgoverlay">
                        <a href="portfolio-single.html" class="loadcontent" rel="lorem-ipsum"><img src="resources/images/portfolio/thumb-work_04.jpg" alt="Work 04"/></a>
                    </div>
                    <div class="portfolio-meta">
                    	<h5><a href="portfolio-single.html" class="loadcontent" rel="lorem-ipsum"><strong>Lorem ipsum</strong></a></h5>
                        <span class="portfolio-categories">Web</span>
                    </div>
             	 </div>
                
                <div class="masonry_item portfolio-entry motion">
                    <div class="imgoverlay">
                        <a href="portfolio-single.html" class="loadcontent" rel="kasd-gubergren"><img src="resources/images/portfolio/thumb-work_05.jpg" alt="Work 05"/></a>
                    </div>
                    <div class="portfolio-meta">
                    	<h5><a href="portfolio-single.html" class="loadcontent" rel="kasd-gubergren"><strong>Kasd gubergren</strong></a></h5>
                        <span class="portfolio-categories">Motion</span>
                    </div>
              	</div>
                
                <div class="masonry_item portfolio-entry web">
                    <div class="imgoverlay">
                        <a href="portfolio-single.html" class="loadcontent" rel="accusam-et-justo"><img src="resources/images/portfolio/thumb-work_06.jpg" alt="Work 06"/></a>
                    </div>
                    <div class="portfolio-meta">
                    	<h5><a href="portfolio-single.html" class="loadcontent" rel="accusam-et-justo"><strong>Accusam et justo</strong></a></h5>
                        <span class="portfolio-categories">Web</span>
                    </div>
              	</div>
                
                <div class="masonry_item portfolio-entry print">
                    <div class="imgoverlay">
                        <a href="portfolio-single.html" class="loadcontent" rel="eirmod-tempor"><img src="resources/images/portfolio/thumb-work_07.jpg" alt="Work 07"/></a>
                    </div>
                    <div class="portfolio-meta">
                    	<h5><a href="portfolio-single.html" class="loadcontent" rel="eirmod-tempor"><strong>Eirmod tempor</strong></a></h5>
                        <span class="portfolio-categories">Print</span>
                    </div>
              	</div>
                
                <div class="masonry_item portfolio-entry web">
                    <div class="imgoverlay">
                        <a href="portfolio-single.html" class="loadcontent" rel="invidunt"><img src="resources/images/portfolio/thumb-work_08.jpg" alt="Work 08"/></a>
                    </div>
                    <div class="portfolio-meta">
                    	<h5><a href="portfolio-single.html" class="loadcontent" rel="invidunt"><strong>Invidunt</strong></a></h5>
                        <span class="portfolio-categories">Web</span>
                    </div>
                </div>
            </div><!-- END #masonry -->
        
        </div><!-- END #recentworks -->
        
        
        <div id="latestnews" class="seperator">
        	<h6 class="sectiontitle">Latest News</h6>
            
            <div class="column one_third">
                <div class="entry clearfix">
                    <div class="entry-thumb">
                    	<div id="slider" class="slidercontent">        
                            <div class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <img src="resources/images/blog/post_03.jpg" alt="Blog Post 03"/>
                                    </li>
                                    <li>
                                        <img src="resources/images/blog/post_02.jpg" alt="Blog Post 02"/>
                                    </li>
                                    <li>
                                        <img src="resources/images/blog/post_01.jpg" alt="Blog Post 01"/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>                    
                	
                    <div class="entry-content clearfix">
                    	<div class="entry-meta left_float">
                        	<div class="meta_type"><a href="blog-single.html" class="type_gallery"></a></div>
                        </div>
                        
                        <div class="entry-info right_float">
                            <div class="post-headline">
                                <h4><strong><a href="blog-single.html">Consectetuer adipiscing elit</a></strong></h4>
                                <span class="entry-date">March 15, 2012</span>
                            </div>
                            <p>
                            Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem.
                            </p>
                               
                        </div>
                    </div>
                </div>
            </div>
                        
            <div class="column one_third">
                <div class="entry clearfix">
                    <div class="entry-thumb">
                    	<div class="embeddedvideo">
                    		<iframe src="http://player.vimeo.com/video/31738112?title=0&amp;byline=0&amp;portrait=0" width="400" height="225" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                        </div>
                    </div>                    
                	
                    <div class="entry-content clearfix">
                    	<div class="entry-meta left_float">
                        	<div class="meta_type"><a href="blog-single.html" class="type_video"></a></div>
                        </div>
                        
                        <div class="entry-info right_float">
                            <div class="post-headline">
                                <h4><strong><a href="blog-single.html">Nullam dictum</a></strong></h4>
                                <span class="entry-date">February 18, 2012</span>
                            </div>
                            <p>
                            Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                            </p>
                               
                        </div>
                    </div>
                </div>
            </div>
                        
            <div class="column one_third last">
                <div class="entry clearfix">
                    <div class="entry-thumb">
                    
                    	<script type="text/javascript">
							$(document).ready(function(){
								if($().jPlayer) {
									$("#jquery_jplayer").jPlayer({
										ready: function () {
											$(this).jPlayer("setMedia", {
												mp3: "resources/audio/test.mp3",
												oga: "resources/audio/test.ogg",
												end: ""
											});
										},
										swfPath: "resources/jplayer",
										cssSelectorAncestor: "#jp_interface",
										supplied: "oga,mp3,  all"
									});
								
								}
							});
						</script>
					
						<div id="jquery_jplayer" class="jp-jplayer jp-jplayer-audio"></div>
			
						<div class="jp-audio-container">
							<div class="jp-audio">
								<div class="jp-type-single">
									<div id="jp_interface" class="jp-interface">
										<ul class="jp-controls">
			
											<li><div class="seperator-first"></div></li>
											<li><div class="seperator-second"></div></li>
											<li><a href="#" class="jp-play" tabindex="1">play</a></li>
											<li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
											<li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
											<li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
										</ul>
			
										<div class="jp-progress-container">
											<div class="jp-progress">
												<div class="jp-seek-bar">
													<div class="jp-play-bar"></div>
												</div>
											</div>
										</div>
										<div class="jp-volume-bar-container">
											<div class="jp-volume-bar">
												<div class="jp-volume-bar-value"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> 
                        
                    </div>                    
                	
                    <div class="entry-content clearfix">
                    	<div class="entry-meta left_float">
                        	<div class="meta_type"><a href="blog-single.html" class="type_audio"></a></div>
                        </div>
                        
                        <div class="entry-info right_float">
                            <div class="post-headline">
                                <h4><strong><a href="blog-single.html">Vivamus elementum</a></strong></h4>
                                <span class="entry-date">February 03, 2012</span>
                            </div>
                            <p>
                            	Venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div><!-- END #latestnews -->
        
	</div> <!-- END #main_inner -->     
</section> <!-- END #main -->