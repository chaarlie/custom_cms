<?php 

include('classes/ZerosDesign.php');
include('includes/header.php');
include('classes/DatabaseOperations2.php');

$dbOp = new DatabaseOperations2();

$columns = 4;// por default 
$columns = isset($_GET['columns'])? htmlspecialchars($_GET['columns']): 4

?>
<body id="blog">
<div id="page">


<header id="header">
    <div class="header_inner wrapper">
    
        <div class="header_top clearfix">
            <div id="logo" class="left_float">
                <a class="logotype" href="index.php"><img src="resources/images/logo.png" alt="Logotype" width="80" height="50"></a>  
            </div>
            
            <nav id="nav" class="right_float">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">Nosotros</a></li>                   
                    <li><a href="portfolio.php" >Folio</a>
                        <ul>
                            <li><a href="portfolio.php?columns=4">4 Columnas</a></li>
                            <li><a href="portfolio.php?columns=3" class="active">3 Columnas</a></li>
                            <li><a href="portfolio.php?columns=2">2 Columnas</a></li>                            
                      </ul>
                    </li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contacto</a></li>
                </ul>
            </nav>
            
        </div>

         <div class="header_tagline seperator-section">
            <h1><strong>Business Branding</strong></h1>
             <h3></h3> 
        </div>
       
    </div>  
</header>


<section id="pagetitle">
    <div class="pagetitle_inner wrapperoverlay">
        <h2><strong>Portafolio</strong></h2>
    </div>
</section>


<section id="main">
    <div class="main_inner wrapper clearfix">
        
        <article class="seperator">
        
            <ul class="filter">
                <li><a class="active" href="" data-option-value="*">All</a></li>
                <?php
                
                $result = $dbOp->select('*', 'portfolio_filters', '');
                foreach ($result as $key ) {  
                
                ?>

                <li><a href="" data-option-value=".<?php echo $key[1]; ?>"><?php echo $key[1]; ?></a></li>
                <?php
                
                }
                
                ?>
            </ul>
            
            <div id="masonry" class="portfolio-entries columns<?php echo $columns ?> clearfix">
                <?php

                $result = $dbOp->select('', '', 'SELECT portfolio_items_id,option_value,img_src,alt,title FROM portfolio_filters JOIN portfolio_items WHERE portfolio_items.portfolio_filters_id = portfolio_filters.portfolio_filters_id  GROUP BY portfolio_items_id');
                foreach ($result as $key ) {  
                
                ?>
                <div class="masonry_item portfolio-entry post <?php echo $key[1]; ?>">

                    <div class="imgoverlay">                        
                        <a href="portfolio_single.php?id=<?php echo $key[0]; ?>"><img src="resources/images/portfolio/<?php echo $key[2]; ?>" alt="<?php echo $key[3]; ?>"/></a>
                    </div>
                    <div class="portfolio-meta">
                        <h5><a href="portfolio-single.html"><strong><?php echo $key[4]; ?></strong></a></h5>
                        <span class="portfolio-categories"><?php echo $key[1]; ?></span>
                    </div>
                </div>
                <?php
                
                }
                
                ?>
                
            </div><!-- END #masonry --> 
        </article>
               
    </div> <!-- END #main_inner -->     
</section> <!-- END #main -->

<?php
include('includes/footer2.php');
?>