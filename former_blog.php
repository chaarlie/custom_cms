<?php 
include('classes/ZerosDesign.php');
include('includes/header.php');



$dbOp = new DatabaseOperations();

$mysqli = $dbOp->connection();
?>

<section id="pagetitle">
	<div class="pagetitle_inner wrapperoverlay">
		<h2><strong>Blog</strong><span class="tagline">Our latest News with sidebar</span></h2>
    </div>
</section>


<section id="main">
    <div class="main_inner wrapper clearfix">        
        <article id="maincontent" class="left_float">
		
			<?php
			$result=$dbOp->select('*','articles',true,'',$mysqli);
			foreach ($result as $key){
				$article_id = $key[0];
			?>
        	<div class="entry clearfix">
                <div class="entry-meta left_float clearfix">
                    <div class="meta_type"><a href="" class="type_gallery"></a></div>
                    <div class="meta_date"><b><?php echo $key[2] ?></b></div>                    
                    <div class="meta_tags"><?php echo $key[3] ?></div>
                    <div class="meta_readmore"><a href="blog-single.html" class="color readmore"><span class="readmoreicon">+</span>read more</a></div>
                </div>
                
                <div class="entry-content right_float">
					<div class="entry-thumb">
					                   									
                        <div id="slider" class="slidercontent">        
                            <div class="flexslider">
                                <ul class="slides">
                                    <?php
                                        $article_imgs = $dbOp->select('','',true,"SELECT img_src FROM article_imgs NATURAL JOIN articles WHERE article_imgs.articles_id = $article_id",$mysqli); 
										//$article_imgs = $dbOp->select('','',"",true,'',$mysqli); 
                                        if (count($article_imgs) > 0){                                                                            
                                            foreach ($article_imgs as $img){
                                                                                                                                                                                          
                                    ?>

                                    <li>
                                        <img src="resources/images/blog/<?php  echo $img[0] ?>"/>
                                    </li>
                                    <?php
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
					<?php
                        $article_audiovisual = $dbOp->select('','',true,"SELECT element_src,element_type FROM audiovisual_elements NATURAL JOIN articles WHERE audiovisual_elements.articles_id = $article_id",$mysqli); 
						//$article_audiovisual = $dbOp->select('',"",true,'',$mysqli); 
                         if (count($article_audiovisual) > 0){
                            $audio_formats = array('audio/mpeg','audio/mp4','audio/ogg','audio/wav');
                            $video_formats = array('video/ogg', 'video/mp4','video/x-m4v','audio/mp4');

                            foreach ($article_audiovisual as $audiovisual){
                                if(in_array($audiovisual[1], $video_formats)){


					?>
								<script type="text/javascript">
											$(document).ready(function(){
												if($().jPlayer) {
													$("#jquery_jplayer").jPlayer({
														ready: function () {
															$(this).jPlayer("setMedia", {
																mp4: "http://localhost/zeros_design/resources/audiovisuals/<?php echo $audiovisual[0] ?>"                                                
															});
														},
														//swfPath: "files/jplayer",
														cssSelectorAncestor: "#jp_interface",
														supplied: "mp4"
													});                                
												}
											});
										</script>

									
										<div id="jquery_jplayer" class="jp-jplayer jp-jplayer-audio"></div>
							
										<div class="jp-audio-container">
											<div class="jp-audio">
												<div class="jp-type-single">
													<div id="jp_interface" class="jp-interface">
														<ul class="jp-controls">
							
															<li><div class="seperator-first"></div></li>
															<li><div class="seperator-second"></div></li>
															<li><a href="#" class="jp-play" tabindex="1">play</a></li>
															<li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
															<li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
															<li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
														</ul>
							
														<div class="jp-progress-container">
															<div class="jp-progress">
																<div class="jp-seek-bar">
																	<div class="jp-play-bar"></div>
																</div>
															</div>
														</div>
														<div class="jp-volume-bar-container">
															<div class="jp-volume-bar">
							
																<div class="jp-volume-bar-value"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php
									}
									?>
					<?php 
                        
								
								if(in_array($audiovisual[1], $audio_formats)){ 

                    ?>
									<div class="entry-thumb">
									
										<script type="text/javascript">
											$(document).ready(function(){
												if($().jPlayer) {
													$("#jquery_jplayer").jPlayer({
														ready: function () {
															$(this).jPlayer("setMedia", {
																mp3: "resources/audiovisuals/<?php echo $audiovisual[0] ?>",
																ogg: "resources/audiovisuals/<?php echo $audiovisual[0] ?>"  
															});
														},
														//swfPath: "files/jplayer",
														cssSelectorAncestor: "#jp_interface",
														supplied: "mp4,ogg"
													});                                
												}
											});
										</script>

									
										<div id="jquery_jplayer" class="jp-jplayer jp-jplayer-audio"></div>
							
										<div class="jp-audio-container">
											<div class="jp-audio">
												<div class="jp-type-single">
													<div id="jp_interface" class="jp-interface">
														<ul class="jp-controls">
							
															<li><div class="seperator-first"></div></li>
															<li><div class="seperator-second"></div></li>
															<li><a href="#" class="jp-play" tabindex="1">play</a></li>
															<li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
															<li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
															<li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
														</ul>
							
														<div class="jp-progress-container">
															<div class="jp-progress">
																<div class="jp-seek-bar">
																	<div class="jp-play-bar"></div>
																</div>
															</div>
														</div>
														<div class="jp-volume-bar-container">
															<div class="jp-volume-bar">
							
																<div class="jp-volume-bar-value"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>        
									</div>
								<?php
								}
								?>
						<?php
							}
						}
						?>
                    <div class="entry-info">
						<div class="post-headline">
							<h3><strong><a href="blog-single.html"><?php  echo $key[5] ?></a></strong></h3>
						</div>
						<p><?php  
						echo $key[4] ;                                                                  
						?></p>
					</div>
					
                </div>
            

            
        <?php
		}
		?>
            
            </div></div>
            <div id="pagination" class="right_float">
            	<a href="" class="nav-next">Previous Entries</a>
            	<a href="" class="nav-prev">Next Entries</a>
            </div> <!-- END #pagination -->
        
        </article>
        
        <aside id="sidebar" class="right_float">
        	<section class="sidebar_section seperator">
            	<div class="widget">
                    <h6 class="sectiontitle">Blog Categories</h6>
                    <div id="menu-widget" >
                        <ul>
                            <li><a href="">Creative</a></li>
                            <li><a href="">Web</a></li>
                            <li><a href="">Architecture</a></li>
                            <li><a href="">Design</a></li>
                            <li><a href="">Motion</a></li>
                        </ul>
                    </div>
                </div>
            </section>
            
            <section class="sidebar_section seperator">
            	<div class="widget">
                    <h6 class="sectiontitle">Text Widget</h6>
                    <div class="text-widget">
                        <p>
                        Nullam vulputate euismod urna non pharetra. Phasellus blandit mattis ipsum, ac laoreet lorem lacinia et. Cras et ligula libero. Quisque quis magna vitae ipsum consequat varius in ut ante. Maecenas a mi nibh, eu euismod orci. 
                    </p>
                    </div>
                </div>
            </section>
            
            <section class="sidebar_section seperator">
            	<div class="widget">
                    <h6 class="sectiontitle">Flickr Feed</h6>
                    <div id="flickr-widget">
                        <ul class="flickr-list">
                             <li><a href=""><img src="files/images/portfolio/thumb-work_01.jpg"></a></li>
                            <li><a href=""><img src="files/images/portfolio/thumb-work_02.jpg"></a></li>
                            <li><a href=""><img src="files/images/portfolio/thumb-work_03.jpg"></a></li>
                            <li><a href=""><img src="files/images/portfolio/thumb-work_04.jpg"></a></li>
                            <li><a href=""><img src="files/images/portfolio/thumb-work_05.jpg"></a></li>
                            <li><a href=""><img src="files/images/portfolio/thumb-work_06.jpg"></a></li>
                            <li><a href=""><img src="files/images/portfolio/thumb-work_07.jpg"></a></li>
                            <li><a href=""><img src="files/images/portfolio/thumb-work_08.jpg"></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            
            <section class="sidebar_section seperator">
            	<div class="widget">
            	<h6 class="sectiontitle">Latest Tweets</h6>
            		<div id="twitter-widget">
						<ul class="tweet-list">
                			<li><span class="tweet_time"><a href="">about 5 hours ago</a></span> <span class="tweet_text">Nullam id dolor id nibh ultricies vehicula ut id elit.</span></li>
                			<li><span class="tweet_time"><a href="">about 8 hours ago</a></span> <span class="tweet_text">Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</span></li>
                			<li><span class="tweet_time"><a href="">about 2 days ago</a></span> <span class="tweet_text">Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem.</span></li>
                		</ul>
                    </div>
                </div>
            </section>
        </aside>
        
        
	</div> <!-- END #main_inner -->     
</section> <!-- END #main -->

<?php

include('includes/footer.php');
?>