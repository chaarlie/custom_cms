<?php 
include('classes/ZerosDesign.php');
include('includes/header.php');

$dbOp = new DatabaseOperations();
$mysqli = $dbOp->connection();

if(isset($_GET['id']))
    $id = $mysqli->escape_string(htmlspecialchars($_GET['id'])) ;
    $id = intval($id) > 0 ? $id : 1;

    $next = $id + 1;
    $previous = $id - 1;

?>

<body id="portfolio">
<div id="page">


<header id="header">
    <div class="header_inner wrapper">
    
        <div class="header_top clearfix">
            <div id="logo" class="left_float">
                <a class="logotype" href="index.php"><img src="resources/images/logo.png" alt="Logotype" width="80" height="50"></a>  
            </div>
            
            <nav id="nav" class="right_float">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">Nosotros</a></li>                   
                    <li><a href="portfolio.php" >Folio</a>
                        <ul>
                            <li><a href="portfolio.php?columns=4">4 Columnas</a></li>
                            <li><a href="portfolio.php?columns=3" class="active">3 Columnas</a></li>
                            <li><a href="portfolio.php?columns=2">2 Columnas</a></li>                            
                      </ul>
                    </li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contacto</a></li>
                </ul>
            </nav>
            
        </div>

         <div class="header_tagline seperator-section">
            <h1><strong>Business Branding</strong></h1>
             <h3></h3> 
        </div>
       
    </div>  
    <script type="text/javascript">
        $(function(){
                $("div.meta_likes").click(function(event){
        event.preventDefault();
        var id = $(this).attr("value");
        
        $.ajax({
            method:'get',
            url:'increment_articles.php',
            data:{id:id,likes:true},            
            success:function(){
                var new_like = parseInt($("#meta_likes_"+ id + " a").html());
                new_like++;
                //un workaround: aparentemente jQuery no me retorna el valor
                document.getElementById("meta_likes_"+id).childNodes.item(0).innerHTML = new_like;

                
                
            }
        });
        

    });
        })
    </script>
</header>


<section id="pagetitle">
    <div class="pagetitle_inner wrapperoverlay">
        <h2><strong>Portfolio</strong><!-- <span class="tagline">What we've done so far</span> --></h2>
    </div>
</section>


<section id="main">
    <div class="main_inner wrapper clearfix">
        
        <article id="maincontent" class="left_float">
            <div id="slider" class="slidercontent">        
                <div class="flexslider">
                    <ul class="slides">
                       
                             <?php

                             $result = $dbOp->select('' ,'',true,"SELECT COUNT(portfolio_items_id) FROM portfolio_items",$mysqli );
                             $total_items = $result[0][0];                            

                             $result = $dbOp->select('' ,'',true,"SELECT img_src,portfolio_items_id FROM  portfolio_items WHERE portfolio_items_id = $id",$mysqli );

                             if ($result ):
                             ?>
                                <li>
                                    <div class="imgoverlay"><a href="resources/images/portfolio/<?php echo $result[0][0] ?>" class="openfancybox" rel="gallery"><img src="resources/images/portfolio/<?php echo $result[0][0] ?>" /></a></div>
                                </li>
                                <?php
                                $result = $dbOp->select('' ,'',true,"SELECT img_src,portfolio_items_id FROM  portfolio_single_item_imgs WHERE portfolio_items_id = $id",$mysqli );
                                foreach ($result as $key ):
                                ?>
                                     <li>
                                        <div class="imgoverlay"><a href="resources/images/portfolio/<?php echo $key[0] ?>" class="openfancybox" rel="gallery"><img src="resources/images/portfolio/<?php echo $key[0] ?>" /></a></div>
                                    </li>
                            <?php
                                endforeach;
                            endif;
                            ?>
                        
                    </ul>
                </div>
            </div>
            
        </article>
        
        <aside id="sidebar" class="right_float">
            <section class="sidebar_section">
                <!--<h3><strong>Fermentum massa justo</strong></h3>-->
                <?php
                $result = $dbOp->select('' ,'',true,"SELECT paragraph,portfolio_items_id FROM  portfolio_items WHERE portfolio_items_id = $id",$mysqli );
                if ($result )
                    foreach ($result as $key ):
                ?>
                        <p><?php 
                        echo nl2br($key[0]);
                        
                        ?></p>
                <?php
                    endforeach;
                ?>
                <p>
                    <!--<a href="" class="themebutton">See Website</a>-->
                </p>
            </section>
            
            <section class="sidebar_section">
                <div id="pagination">
                    <a href="portfolio_single.php?id=<?php  echo $previous < 1 ? $total_items : $previous ?>" id="nav-prev" class="loadcontent">Anterior</a>
                    <a href="portfolio_single.php?id=<?php  echo $next > $total_items? 1 : $next ?>" id="nav-next" class="loadcontent">Siguiente</a>
                </div> <!-- END #pagination -->
            </section>
        </aside>
        
    </div> <!-- END #main_inner -->     
</section> <!-- END #main -->

<?php

include('includes/footer.php');
?>