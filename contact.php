<?php 
include('classes/ZerosDesign.php');
include('includes/header.php');



$dbOp = new DatabaseOperations();

$mysqli = $dbOp->connection();

?>

<body id="blog">
<div id="page">



<header id="header">
    <div class="header_inner wrapper">
    
        <div class="header_top clearfix">
            <div id="logo" class="left_float">
                <a class="logotype" href="index.php"><img src="resources/images/logo.png" alt="Logotype" width="80" height="50"></a>  
            </div>
            
            <nav id="nav" class="right_float">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">Nosotros</a></li>                   
                    <li><a href="portfolio.php" >Folio</a>
                        <ul>
                            <li><a href="portfolio.php?columns=4">4 Columnas</a></li>
                            <li><a href="portfolio.php?columns=3" class="active">3 Columnas</a></li>
                            <li><a href="portfolio.php?columns=2">2 Columnas</a></li>                            
                      </ul>
                    </li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contacto</a></li>
                </ul>
            </nav>
            
        </div>

         <div class="header_tagline seperator-section">
            <h1><strong>Business Branding</strong></h1>
             <h3></h3> 
        </div>
       
    </div>  
    <script type="text/javascript">
        $(function(){
                $("div.meta_likes").click(function(event){
        event.preventDefault();
        var id = $(this).attr("value");
        
        $.ajax({
            method:'get',
            url:'increment_articles.php',
            data:{id:id,likes:true},            
            success:function(){
                var new_like = parseInt($("#meta_likes_"+ id + " a").html());
                new_like++;
                //un workaround: aparentemente jQuery no me retorna el valor
                document.getElementById("meta_likes_"+id).childNodes.item(0).innerHTML = new_like;

                
                
            }
        });
        

    });
        })
    </script>
</header>


<section id="pagetitle">
    <div class="pagetitle_inner wrapperoverlay">
        <h2><strong>Contacto</strong></h2>
    </div>
</section>

   
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>

<section id="map"></section>

<script type="text/javascript">
  function mapinitialize() {
    // FIND YOUR LATITUDE & LONGITUDE  -> http://itouchmap.com/latlong.html  
    var myLatlng = new google.maps.LatLng(18.463134,-69.919739);
    var myOptions = {
      zoom: 14,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById("map"), myOptions);
    
    var marker = new google.maps.Marker({
        position: myLatlng, 
        map: map,
        title:"Zeros Design"
    });
    
  }
  mapinitialize();
</script>


<section id="main">
    <div class="main_inner wrapper clearfix">
        
        <article>
            <div class="column two_third">
                <h4><strong>Déjenos un mensaje</strong></h4>
                <form id="contact-form" class="checkform" action="" target="contact_send.php" method="post">
                    <div>   <label for="name" class="req">NOMBRE *</label>
                            <input type="text" name="name" class="name" value="NOMBRE *" onFocus="if (this.value == 'NAME *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'NAME *';}"/></div>
                    <div>   <label for="email" class="req">CORREO *</label>
                            <input type="text" name="email" class="email" value="CORREO *" onFocus="if (this.value == 'EMAIL *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'EMAIL *';}"/></div>
                    <div>   <label for="subject">ASUNTO *</label>
                            <input type="text" name="subject" class="subject" value="ASUNTO *" onFocus="if (this.value == 'SUBJECT') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'SUBJECT';}"/></div>        
                    <div>   <label for="message" class="req">MENSAJE *</label>
                            <textarea name="message" class="message" onFocus="if (this.value == 'MENSAJE *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'MENSAJE *';}" rows="15" cols="50">MENSAJE *</textarea></div>
                    <div><input class="submit" type="submit" value="Enviar" name="submit_form" /></div>                    
                </form>
                <p id="form-note"><span class="error_icon">Error</span><span class="error_message"><strong>¡Por favor, revise los datos!</strong></span></p>
            </div>
            
            <div class="column one_third last">

                <h4><strong>Información de Contacto</strong></h4>
                <p>
                <?php
                 $result = $dbOp->select('*' ,'contact_infos',true,'',$mysqli );
                ?>
                <strong>Zeros</strong><br />
                <?php echo $result[0][1]; ?>
                </p>
                <p>
                Tel: <?php echo $result[0][2]; ?><br />
                
                Website: <a href="<?php echo $result[0][1]; ?>"><?php echo $result[0][3]; ?></a><br />
                <?php
                 $result = $dbOp->select('' ,'',true,' select * from contact_info_emails where contact_info_emails.contact_infos_id = 1',$mysqli );
                 foreach ($result as $key ) {  
                ?>
                Correo: <a href=""><?php echo $key[2]?></a>
                </p>
                <?php
                }
                ?>
            </div>
            <div class="clear"></div>
            
        </article>
        
    </div> <!-- END #main_inner -->     
</section> <!-- END #main -->

<?php
include('includes/footer.php');
?>