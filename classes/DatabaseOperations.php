<?php
class DatabaseOperations{

	

	public static function connection(){

		 $host = "localhost";
		 $database = "zeros_design";
		 $user = "root";
		 $password = "";


		$mysqli = new mysqli($host,$user,$password,$database);

		if($mysqli->connect_error) die("Ha ocurrido un error al conectarse a la base de datos. Error".$mysqli->error);

		return $mysqli;		
	}

	public static function create_table($table_name,$mysqli){		
		if(is_string($table_name)){
				$query = "create table ".$table_name;
				if(!$executed = $mysqli->query($query)) die("ha ocurrido un error mientras se creaba la tabla. Error:".$mysqli->error);

		}

	}	

public static function update($table,$values,$where_statement,$mysqli){
		/*
			$values: cada elementent es un registro de la base de datos!
		*/

		if(is_string($table) )
			$update = "";

		$select = "select * from $table limit 1";

		if(!$result = $mysqli->query($select)) die("Ha ocurrido un error al seleccionar data1. Error:".$mysqli->error);

		if(!$row = $result->fetch_assoc()) die("Ha ocurrido un error al seleccionar data2. Error:".$mysqli->error);
		
		$keys = array_keys($row);
				
		foreach ($values as $key => $value) {
			$str=  "UPDATE $table SET  ";

			for($i = 0; $i < count($keys)-1; $i++){
			
				if ($value[$i+1] =='') {
					continue;
				}
				$str.=" ".$keys[$i+1]." =  '".$value[$i+1]."', ";	
			 }

			 if($str!=="UPDATE $table SET  "){
			 	$str = substr($str, 0,strlen($str)-2);			 	
			 	$str.=" where  ".$keys[0]." = ".$value[0]." ";
			 	
			}
			else
				$str='';
		
			 $update .= $str;
			
				if ($update!=='')
			 		if(!$result = $mysqli->query($update)) die("Ha ocurrido un error al actualizar data. Error:".$mysqli->error);
			 		
					//echo "$update";
			 

			$update = '';


		}		
		return true;
	}

	public static function select($col,$table,$where,$select_statement,$mysqli){
		$result_data = array();

		if(is_string($table) && is_string($col))
			$query = "select $col from $table ";


		if (is_string($select_statement)) {
			if($select_statement!='')
				$query = $select_statement;
		}

		if(!$result = $mysqli->query($query)) die("Ha ocurrido un error al seleccionar data. Error:".$mysqli->error);
		//echo "$query";
		
		for ($i = 0;$row = $result->fetch_assoc();$i++) {
			$keys = array_keys($row);
			$result_data[$i] = array();

			for ($j = 0; $j < count($keys); $j++) { 
				array_push($result_data[$i], $row[$keys[$j]]) ;
			}
			
		}

		return $result_data;
	}

	public static function insert($table,$values,$mysqli){

            $insert = 'insert into '.$table;

            for($i = 0; $i < count($values); $i++){

                if(is_string($values[$i]))
                    $values[$i] = '"'.$values[$i].'"';
            }

            $values = implode(',',$values);
            $insert .= ' values ('.$values.')';

            return $mysqli->query($insert)  ? true : false;        

            //echo "<br />$insert<br />";
    }
    
    public  static function delete($table,$where,$mysqli){
    	$delete =  'delete from  '.$table.' where '.$where;
    	return $mysqli->query($delete) ? true : false;    	
    }
}
?>