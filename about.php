<?php 

include('classes/ZerosDesign.php');
include('includes/header.php');
include('classes/DatabaseOperations2.php');

$dbOp = new DatabaseOperations2();

?>

<body id="blog">
<div id="page">


<header id="header">
    <div class="header_inner wrapper">
    
        <div class="header_top clearfix">
            <div id="logo" class="left_float">
                <a class="logotype" href="index.php"><img src="resources/images/logo.png" alt="Logotype" width="80" height="50"></a>  
            </div>
            
            <nav id="nav" class="right_float">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">Nosotros</a></li>                   
                    <li><a href="portfolio.php" >Folio</a>
                        <ul>
                            <li><a href="portfolio.php?columns=4">4 Columnas</a></li>
                            <li><a href="portfolio.php?columns=3" class="active">3 Columnas</a></li>
                            <li><a href="portfolio.php?columns=2">2 Columnas</a></li>                            
                      </ul>
                    </li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contacto</a></li>
                </ul>
            </nav>
            
        </div>

         <div class="header_tagline seperator-section">
            <h1><strong>Business Branding</strong></h1>
             <h3></h3> 
        </div>
       
    </div>  
    <script type="text/javascript">
    </script>
</header>


<section id="pagetitle">
    <div class="pagetitle_inner wrapperoverlay">
        <h2><strong>Acerca de Nosotros</strong></h2>
    </div>
</section>


<section id="main">
    <div class="main_inner wrapper clearfix">
        
        <article>
                
            <!-- <h2 class="page-description"><i> Something blah blah</i></h2> -->

            <div class="seperator-section"></div>
              <?php

              $result = $dbOp->select('*', 'about_main_contents', '');
              
              ?>
              <div class="column one_third"><img title="about" src="resources/images/about/<?php echo $result[0][1] ?>" alt="" /></div>
              <div class="column one_third">
                <h4><strong>Iris - Web Agency</strong></h4>
                <?php

                $result = $dbOp->select('', 'about_main_paragraphs', 'SELECT content FROM about_main_paragraphs LIMIT  1');

                foreach ($result as $key) {

                ?>
                <p><?php echo nl2br($key[0]);?></p>
                <?php

                }

                ?>
                <br />
            </div>
            
            <div class="column one_third last">
                <h4><strong>Skills</strong></h4>
                 <?php
                 
                 $result = $dbOp->select('', '', ' SELECT name,val FROM about_skills ');
                 
                 foreach ($result as $key) {
                 
                 ?>
                <div class="skill">
                    <span class="skill_name"><strong>><?php echo $key[0];?></strong></span>
                    <div class="skill_bar"><div class="skill_active" style="width: <?php echo $key[1];?>%;"></div><span><i><?php echo $key[1];?>%</i></span></div>
                </div>
                <?php

                }

                ?>
                
            </div><div class="clear"></div>
            
            <div class="seperator">
                <h6 class="sectiontitle">Nuestro Equipo</h6>
            </div>
            <?php

            $result = $dbOp->select('*', 'about_team_members', '');

            foreach ($result as $key) {

            ?>
            <div id="team_members">
                <div class="column one_fourth">
                    <div class="team-member">
                        <img src="resources/images/about/<?php echo $key[1];?>" alt="Person"/>
                        <div class="team-meta">
                            <h5><strong><?php echo $key[2];?></strong></h5>
                            <span class="team-title"><i><?php echo $key[3];?></i></span>
                        </div>
                        <p>
                        <?php echo $key[4];?>
                        </p>
                    </div>
                </div>
            </div>
            <?php
            
            }
            
            ?>
            
        </article>
               
    </div> <!-- END #main_inner -->     
</section> <!-- END #main -->

<?php
include('includes/footer2.php');
?>