<?php

session_start();

include('classes/DatabaseOperations.php');
include('includes/answers.php');
include('includes/edits_header.php');
//include('includes/header.php');

if (isset($_SESSION['user'])){

	//include('includes/nav.php');

	$dbOp = new DatabaseOperations();
	$mysqli = $dbOp->connection();

	if(isset($_POST['contact_address']) || 
		isset($_POST['contact_tel']) ||
		isset($_POST['contact_site']) ||
		isset($_POST['contact_emails']) 
	){		
		
		$contact_address = $mysqli->real_escape_string(htmlspecialchars($_POST['contact_address']));
		$contact_tel = $mysqli->real_escape_string(htmlspecialchars($_POST['contact_tel'] ));
		$contact_site = $mysqli->real_escape_string(htmlspecialchars($_POST['contact_site'] ));
		$contact_emails = $_POST['contact_emails'];
		
		$info = array(array(1,$_POST['contact_address'],$_POST['contact_tel'],$_POST['contact_site']));
		$dbOp->update('contact_infos',$info,'',$mysqli);

		$info = array(array(1,1,$contact_emails[0]),array(2,1,$contact_emails[1]));
		$dbOp->update('contact_info_emails',$info,'',$mysqli);

		
		do_success_answer("LA INFORMACION<br /> SE HA PROCESADO");
	}	
}

else{
    echo "NO ESTA LOGUEADO.PARA HACERLO CLICK <a href='login.php'>AQUI</a>";
    //include('includes/footer.php');
}
?>
<button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>