<?php
include('includes/edits_header.php');

session_start();

if (isset($_SESSION['user'])){

    include('classes/DatabaseOperations.php');
    include('classes/SimpleImage.php');
    include('includes/upload_file.php');
    include('includes/sanitize_info.php');

    $index_banners_imgs = $_FILES["index_banners_imgs"];
    $index_banners_titles = $_POST["index_banners_titles"];
    $index_banners_texts = $_POST["index_banners_texts"];
    $index_banners_ids = $_POST["index_banners_ids"];

    $simple_image = new SimpleImage();
    $dbOp = new DatabaseOperations();
    $mysqli = $dbOp->connection();

    

    /*
    index_banners
    */

    $dir = '../resources/images/';
    $img_sources = save_file($index_banners_imgs,$dir,1060,350,$simple_image,'img' );
    $info = array();

    for ($i=0; $i < count($index_banners_ids) ; $i++) { 
        $info[$i]=array();
        array_push($info[$i], $mysqli->real_escape_string(htmlspecialchars ($index_banners_ids[$i])));
        array_push($info[$i], $mysqli->real_escape_string(htmlspecialchars ($index_banners_titles[$i])));
        array_push($info[$i], $mysqli->real_escape_string(htmlspecialchars ($img_sources[$i])));
        array_push($info[$i], $mysqli->real_escape_string(htmlspecialchars ($index_banners_texts[$i])));
    }

    $info = sanitize_info($info,$mysqli);   

    $result = $dbOp->update('index_banners',$info,'',$mysqli);
 
}

else
    echo "NOT_LOGGED_IN";

include('includes/edits_footer.php');
?>

<button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>


