<?php

session_start();

include('classes/DatabaseOperations.php');

include('includes/edits_header.php');
include('includes/upload_file.php');
include('classes/simpleimage.php');


if (isset($_SESSION['user'])){


	$dbOp = new DatabaseOperations();
	$mysqli = $dbOp->connection();

	if(isset($_POST['article_title']) || 
		isset($_POST['article_description']) ||
		isset($_POST['article_content']) ||
		isset($_FILES['article_imgs']) 
	){		
		
		$article_title = $mysqli->real_escape_string(htmlspecialchars($_POST['article_title']));
		$article_description = $mysqli->real_escape_string(htmlspecialchars($_POST['article_description'] ));
		$article_content = $mysqli->real_escape_string(htmlspecialchars($_POST['article_content'] ));
		$article_imgs = isset($_FILES['appent_article_imgs']) ? $_FILES['appent_article_imgs']: array();
		$article_creation = $mysqli->real_escape_string(htmlspecialchars($_POST['article_creation'] ));
		$article_category = $mysqli->real_escape_string(htmlspecialchars($_POST['article_category'] ));
		$article_id = $mysqli->real_escape_string(htmlspecialchars($_POST['article_id'] ));

		$parts = explode('/', $article_creation);
		$article_creation  = "$parts[2]-$parts[0]-$parts[1]";

		$info = array(array($article_id,$article_content ,$article_creation ,$article_category,$article_description,$article_title));
		$dbOp->update('articles',$info,'',$mysqli);

		if(count($article_imgs) > 0){
			$si = new SimpleImage();
			$imgs = save_file($article_imgs,"../../resources",150,150,$si,"video");	
			$info = array();

			for ($i=0; $i < count($imgs) ; $i++) { 
				$info[]= array('',$article_id,$imgs[$i]);			
			}

			$dbOp->insert('article_imgs',$info,$mysqli);		
		}
			do_success_answer("LA INFORMACION <br /> SE HA PROCESADO")
		?>
		<button ><a href='<?php  echo $_SERVER["HTTP_REFERER"]?>' style="text-decoration:none;color:grey;font-weight:bold">Regresar</a></button>

		<?php
	}

	elseif (isset($del_blog_id)) {
				$id = $mysqli->real_escape_string($_POST['id']);				
		$img = $dbOp->select("","","","SELECT img_src FROM  portfolio_single_item_imgs WHERE portfolio_single_item_imgs_id =$id ",$mysqli);
		$path_delete = "../resources/images/portfolio/".$img[0][0];
		$result = $dbOp->delete("portfolio_single_item_imgs","portfolio_single_item_imgs_id = $id",$mysqli);

		echo $result ? "se ha eliminado correctamente" : "no se pudo eliminar";


		//$path_delete = "../resources/images/portfolio/socks.ph";
		chown($path_delete,1233);
		echo	unlink("$path_delete");			
	}
	else{
		include('includes/header.php');	

		$id = 0;
		$total_rows = 0;

		if(isset($_GET['id']) )
			$id = $_GET['id'];		

		$id = $mysqli->real_escape_string(htmlspecialchars($id));	
?>
<div id='main_container' >	
	<div id="blog_article" >

		<h3>Articulos</h3>
		<div id="blog_category"class="input">
			<form action="blog_category_handler.php" method="post" >
				<?php
				$categories = $dbOp->select('' ,'articles',true,"select * from blog_categories",$mysqli );
				
				foreach ($categories as $category ) :																	
			    ?>
					<ul>
						<li>
							<input type="text" name="blog_category_entries[]" value="<?php echo $category[1]; ?>" />
							<input type="hidden" name="blog_category_entries_ids[]" value="<?php echo $category[0]; ?>" />
						</li>
					
					<?php
					endforeach;
					?>						
					</ul>
					

					<p>Agregar categoria</p>				
						<button id="add_blog_category" class="square"><i class="icon-plus-sign"></i></button>
					<div></div>					

					<br/>
					<br/>
					<button class="medium">Guardar </button>
			</form>
		</div>
		<?php

			$result = $dbOp->select('' ,'articles',true,"select count(*) from articles",$mysqli );
			$total_rows = $result[0][0];
			
			$result = $dbOp->select('' ,'articles',true,"select * from articles where articles_id > $id limit 3 ",$mysqli );			
		
	        foreach ($result as $key ):	         	         

				$fecha = $newDate = date("d/m/Y", strtotime($key[2]));
				$article_no = $key[0]			;
	    ?>
		<div class="">
			<form method="post" action="edit_blog_handler.php" enctype="multipart/form-data"> 
				<div><h5 style="display:inline-table;" >Articulo No. <?php echo $key[0]?></h5><button class="medium red"  style="display:inline-table; margin-left:75px">Eliminar Elemento </button></div>
				<br />
				<p>
					<h6>Titulo</h6>
					<textarea class="title" name="article_title" rows="1" cols="70"><?php  echo $key[5] ?></textarea>
				</p>

				<p>
					<h6>Contenido</h6>
					<textarea class="content" name="article_content" rows="1" cols="70"><?php echo $key[1] ?></textarea>
				</p>
				<p>
					<h6>Catgoria</h6>
					<select class="category" name="article_category">
						<?php									
						foreach ($categories as $category ) :																	
			    		?>					
							<option>
								<?php echo $category[1]; ?>
							</option>					
						<?php
						endforeach;
						?>										
					</select>
				</p>
				<p>
					<h6>Descripcion</h6>
					<textarea class="description" name="article_description" rows="1" cols="70"><?php $key[4] ?></textarea>
				</p>

				<p>
					<h6> Fecha de creacion</h6>
					<input class="creation" type="text" value="<?php echo $fecha ?>" name="article_creation" />
				</p>
				<br/>

				<p>					

					<h6>imagenes del articulo</h6>
					<?php							
					$result = $dbOp->select('' ,'ARTICLE_IMGS',true,"select * from ARTICLE_IMGS,ARTICLES where ARTICLE_IMGS.articles_id = $article_no  ",$mysqli );			
		
	        		foreach ($result as $key ):	         	         

					?>
					<!-- <div class="article_img">
						<img src="../resources/images/<?php echo $key[2] ?>" />
						<button class="medium red" >eliminar </button>
					</div> -->
					  <div id="portfolio_img_container" class="portfolio_img_<?php echo $img[0]?>" >
              			<img src='../resources/images/<?php echo $key[1]?> ' width='200' height='150'/>                              
              			<button class="red" id="delete_portfolio_img" value="<?php echo $img[0]?>"><i class="icon-minus-sign"></i> borrar</button>
            		</div>
					<?php
					endforeach;
					?>

					<br/ ><br/ >
					<button id="blog_article_add_img" class="medium square" style="font-size:150%" value="<?php echo $key[0] ?>"><i class="icon-plus-sign"></i></button>            

					<div id="article_<?php echo $key[0] ?>">
					</div>


				</p>

				<input type="hidden" value="<?php echo $key[0] ?>" name="article_id" />

				<button id="save">Guardar</button> 
			 </form> 
			<br/><br/>
		<?php 
		endforeach;
		?>
        </div>
	    <br/><br/><br/>

		<h5>Agregar nuevo articulo</h5>	
		<button id="blog_article_add" class="medium square" style="font-size:150%" ><i class="icon-plus-sign"></i></button>
		<br/><br/>
	</div>

<?php
	$id += 3;
	if($id > $total_rows) 
		$id = 0;

?>
<button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>



<?php
	}
}
else
    echo "NO ESTA LOGUEADO.PARA HACERLO CLICK <a href='login.php'>AQUI</a>";   
?>

