<?php 

session_start();

include('classes/DatabaseOperations.php');
include('includes/header.php');


if (isset($_SESSION['user'])){

include('includes/nav.php');


$dbOp = new DatabaseOperations();
$mysqli = $dbOp->connection();

?>
<div id="main_container" >
  <button class="medium blue pill" id="user_logout" >Desloguearse</button>

	<form action="edit_portfolio_handler.php" method="post" enctype="multipart/form-data" >	
    <div id="portafolio_filters" style="width:30%">      
      <h3>Filtros del portafolio</h3>
      <ul>
        <?php
        $result = $dbOp->select('*' ,'portfolio_filters',true,'',$mysqli );
  		  $filters = $result;
        foreach ($result as $key ):
        ?>
          <li>
            Filtro:
            <input type="text" name="portfolio_filters[]" value="<?php echo $key[1]?>"/>
            <input type="hidden" name="portfolio_filters_ids[]" value="<?php echo $key[0]?>"/>
          </li>
        <?php
        endforeach;
        ?>
        <li><button class="small blue " id="create_account_button" style="float:right"> Guardar</button></li>
      </ul>
    </div>
    
  </form>

    <br />
    <br />
    <br />

    <div id="portafolio_elements">
      <h3>Elementos del portafolio</h3>
      <?php
      $result = $dbOp->select('','',true,'SELECT portfolio_items_id,portfolio_items.portfolio_filters_id,option_value,img_src,alt,title FROM portfolio_filters JOIN portfolio_items WHERE portfolio_items.portfolio_filters_id=portfolio_filters.portfolio_filters_id  GROUP BY portfolio_items_id',$mysqli ); 
      foreach ($result as $key ):
        $item_id = $key[0];

      
            
      ?>
      <form action='save_portfolio_elements.php' method='post' enctype="multipart/form-data" id='save_<?php echo $key[0]?>' data-ajax="false">
        
        <h5>Elemento <?php echo $item_id?></h5>
        <p>
          Imagen primaria:<br />
          <img src="../resources/images/portfolio/<?php echo $key[3]?>" alt="<?php echo $key[4]?>" width="235" height="170" /> <br/>
          <input type="file" name="portfolio_item_main_img[]" />
          <input type="file" style="visibility:hidden" name="portfolio_item_main_img[]" />
        </p>
        
        <p>
          Filtro:
          <br />
          <select name="portfolio_item_filter">
            <option value="<?php echo $key[1]?>" selected>
         			<?php echo $key[2]?>
         		</option>
         		<?php
         		for ($i=0; $i < count($filters) ; $i++):
              $index = 0;                              
            ?>
              <option value="<?php echo $filters[$i][$index]?>" ><?php echo $filters[$i][$index+1]?></option>
            <?php
            endfor;
            ?>
          </select>
        </p>        
        <p>
          Titulo:
          <br/>
          <input type="text" name="portfolio_item_title" value="<?php echo $key[5]?>"/>
        </p>

        <input type="hidden" name="portfolio_item_id" value="<?php echo $key[0]?>"/> 
        <!-- <input type="hidden" name="portfolio_single_filters_ids[]" value="<?php echo $key[1]?>"/> -->

        <p>Parrafos</p>
        <?php

        $portfolio_element_paragraphs = $dbOp->select('','',true,"SELECT  portfolio_items_id,paragraph FROM  portfolio_items WHERE portfolio_items.portfolio_items_id=$item_id",$mysqli );
        foreach ($portfolio_element_paragraphs as $paragraph ):

        ?>        
          <textarea name="item_paragraph"><?php echo $paragraph[1]?></textarea>          
          <input type="hidden" name="portfolio_items_id[]" value="<?php echo $paragraph[0]?>"/>

        <?php
        endforeach;
        ?>


        <p>Imagenes</p>

        <div id="portfolio_img_main">
          <?php

          $portfolio_element_imgs = $dbOp->select('','',true,"SELECT  portfolio_single_item_imgs_id,img_src FROM  portfolio_single_item_imgs WHERE portfolio_single_item_imgs.portfolio_items_id=$item_id",$mysqli );
          foreach ($portfolio_element_imgs as $img ):

          ?>        
            <div id="portfolio_img_container" class="portfolio_img_<?php echo $img[0]?>" >
              <img src='../resources/images/portfolio/<?php echo $img[1]?> ' width='200' height='150'/>                              
              <button class="red" id="delete_portfolio_img" value="<?php echo $img[0]?>"><i class="icon-minus-sign"></i> borrar</button>
            </div>
   
          <?php
          endforeach;
          ?>
        </div>

        <br/>
        <br/>
        <br/>

        <div id="paragraphs_element_<?php echo $key[0]?>" style="opacity:0">
        </div>


        
       
        <br/>

        <div id="imgs_element_<?php echo $key[0]?>" style="opacity:0">
</div>         <p>             Agregar imagen:<br />         </p> 

        <button id="portfolio_elements_img_add" class="medium square" style="font-size:150%" value="<?php echo $key[0]?>"><i class="icon-plus-sign"></i></button>            

        <br/>
        <br/>

        
        
        <button class="save_button medium blue" id="save_elements" value="<?php echo $key[0]?>">Guardar</button>
        <!--<input type="submit" value="Submit" class="save_button" value="<?php echo $key[0]?>" />-->
        
        
        
        <br/>
        <br/>
        <br/>
        <br/>

</form>
    
      <?php
    	endforeach;
      ?>
    </div>

<button id="add_new_portfolio_element">agrear nuevo elemento</button>
<br/><br/><br/>
    <div id="new_portfolio_element" style="opacity:0">
      <form action="add_new_portfolio_element.php" method="post" enctype="multipart/form-data">
        
          <p>
            Filtro:
            <br />
            <select name="new_portfolio_element_select">
              <option value="<?php echo $key[1]?>" selected>
                <?php echo $key[2]?>
              </option>
              <?php
              for ($i=0; $i < count($filters) ; $i++):
                $index = 0;                              
              ?>
                <option value="<?php echo $filters[$i][$index]?>" ><?php echo $filters[$i][$index+1]?></option>
              <?php
              endfor;
              ?>
            </select>          
          </p>
          <p> imagen: <input type="file" name="new_portfolio_element_img" src=""/></p>             
          <p> texto alternativo: <input type="text" name="new_portfolio_element_alt" /></p>
          <p> titulo: <input type="text" name="new_portfolio_element_title" /></p>
          <p>
            Parrafo:<br/>
            <textarea name="new_portfolio_element_paragaph" rows="4" cols="22"></textarea>
          </p>
          
        </div>
        <br/>
        <br/>
        <br/>
        <button class="medium  " id="submit_portfolio_element">Guardar elemento</button>
        <br/>
        <br/>
     </form>         
</div>

<?php

include('includes/footer.php');

}

else{
    echo "NO ESTA LOGUEADO.PARA HACERLO CLICK <a href='login.php'>AQUI</a>";
    include('includes/footer.php');
}
?>

