<?php 

session_start();

include('includes/header.php');

if(isset($_SESSION['user'])){
	include('classes/ZerosDesign.php');
	include('includes/nav.php');		

	$dbOp = new DatabaseOperations();
	$mysqli = $dbOp->connection();

?>
<div id='main_container' >
	<form action='edit_about_handler.php' method='post'enctype="multipart/form-data" >
		<div id="about_main_contents_container">
			<button class="medium blue pill" id="user_logout" >Desloguearse</button>
			<h3>Contenido principal</h3>
			<?php
				$result = $dbOp->select('*','about_main_contents',true,'',$mysqli);
			?>
			<p>
				<img src="../resources/images/about/<?php echo $result[0][1] ?>" /><br />
			</p>
			<p>
				Imagen:<br/>
				<input type="file" name="about_main_img[]"  />
				 
			</p>
			<input type="hidden" name="about_main_img_id[]" value="<?php echo $result[0][0] ?>" />
 			
		
			<?php
				$result = $dbOp->select('','about_main_paragraphs',true,'SELECT * FROM about_main_paragraphs LIMIT  1',$mysqli);
				foreach ($result as $key):
			?>
					<p>	
						Parrafo <?php echo $key[0] ?>:<br />
						<textarea rows="3" cols="40" name="about_main_paragraphs[]"><?php echo $key[2] ?></textarea>
						<input type="hidden" name="about_main_paragraphs_ids[]" value="<?php echo $key[0] ?>" />
					</p>
			<?php
				endforeach;
			?>
		</div>

		<br/>
		<br/>
		<br/>

		<div id="about_skills_container">
			<h3>Skills</h3>
			<?php
			$result=$dbOp->select('','',true,' select about_skills_id,name,val from about_skills ',$mysqli);
			foreach ($result as $key):
			?>
				<div id="skill_<?php echo $key[0]?>" class="about_skill">
					<p>
						<?php echo $key[1]?>:
						<select  name="about_skill_values[]" >
							<option selected><?php echo $key[2]?></option>
							<?php for ($i=1; $i <101 ; $i++): ?>
								<option ><?php echo $i?></option>
							<?php 
							endfor;
							?>
						</select>
					</p>			
					<p>
						cambiar opcion:<br/>
						<input type="text"  name="about_skill_names[]" value="<?php echo $key[1]?>" />
						<button class="small red" id="about_skill_delete" style="font-size:150%;" value="<?php echo $key[0]?>">-</button>
					</p>
					<input type="hidden" value="<?php echo $key[0]?>" name="about_skill_ids[]" />
				</div>
			<?php
			endforeach;
			?>
			<p>
				<button id="about_skill_add" class="medium square" style="font-size:150%" ><i class="icon-plus-sign"></i></button>
			</p>	
		</div>

		<br/>
		<br/>
		<br/>

		
		<div id="about_members_container">
			<h3>Miembros</h3>
			<?php
			$result=$dbOp->select('*','about_team_members',true,'',$mysqli);
			foreach ($result as $key):
			?>
				<br/>
				<div class="about_members" id="about_members_<?php echo $key[0];?>">
					<h5><?php echo $key[2];?></h5>
					<img src="../resources/images/about/<?php echo $key[1];?>" />                    
					<p>
						<input type="file" name="about_team_imgs[]" />
					</p>
					<p>
						Nombre:<br />
						<input type="text" name="about_team_names[]" value="<?php echo $key[2];?>"/>
					</p>
					<p>
						Posicion:<br />
						<input type="text" name="about_team_positions[]" value="<?php echo $key[3];?>"/>
					</p>
            		<p>
						Descripcion:<br />
						<textarea name="about_team_descriptions[]"><?php echo $key[4];?></textarea>
					</p>
            		<input type="hidden" name="about_team_ids[]" value="<?php echo $key[0];?>" />
            		<br />
            		<button class='about_members_delete' value="<?php echo $key[0];?>">Eliminar</button>
            	</div>

			<?php
			endforeach;
			?>
			<br />
			<br />
			<button id="about_members_add" class="medium square" style="font-size:150%" ><i class="icon-plus-sign"></i></button>
		</div>
		
		<br />
		<br/>
		<br/>

		<button class="medium blue " id="submit">Guardar</button>
		<br />
		<br/>
		<br/>
	</form>
</div>

<?php


}

else{
	echo "NO ESTA LOGUEADO, FAVOR HACERLO AQUI<a href='login.php'>AQUI</a>";
	
}
include('includes/footer.php');
?>