<?php

session_start();

include('classes/DatabaseOperations.php');

include('includes/edits_header.php');


if (isset($_SESSION['user'])){


	$dbOp = new DatabaseOperations();
	$mysqli = $dbOp->connection();

	if(isset($_POST['article_title']) || 
		isset($_POST['article_description']) ||
		isset($_POST['article_content']) ||
		isset($_FILES['article_imgs']) 
	){		
		
		include('includes/upload_file.php');
		include('classes/simpleimage.php');


		

		$article_title = $mysqli->real_escape_string(htmlspecialchars($_POST['article_title']));
		$article_description = $mysqli->real_escape_string(htmlspecialchars($_POST['article_description'] ));
		$article_content = $mysqli->real_escape_string(htmlspecialchars($_POST['article_content'] ));
		$article_imgs = isset($_FILES['appent_article_imgs']) ? $_FILES['appent_article_imgs']: array();
		$article_audiovisual= isset($_FILES['appent_article_audiovisual']) ? $_FILES['appent_article_audiovisual']: array();
		$article_vimeo_video= isset($_POST['appent_article_video'])?$mysqli->real_escape_string(( $_POST['appent_article_video'])):'';
		$update_vimeo_video= isset($_POST['update_article_video'])?$mysqli->real_escape_string(( $_POST['update_article_video'])):'';
		$article_creation = $mysqli->real_escape_string(htmlspecialchars($_POST['article_creation'] ));
		$article_selected_categories = isset($_POST['article_selected_categories'])? $_POST['article_selected_categories'] : null;
    	

		$article_category ="";
		$articles_id = $mysqli->real_escape_string(htmlspecialchars($_POST['articles_id'] ));
		

		$parts = explode('/', $article_creation);
		$article_creation  = "$parts[2]-$parts[0]-$parts[1]";
		$info = array(array($articles_id,$article_content ,$article_creation ,$article_category,$article_description,$article_title,'','','',''));
		$dbOp->update('articles',$info,'',$mysqli);

		if(count($article_imgs) > 0){
			$si = new SimpleImage();
			$imgs = save_file($article_imgs,"../resources/images/blog",350,200,$si,"img");	
			$info = array();

			for ($i=0; $i < count($imgs) ; $i++) { 
				$info[]= array('',$articles_id,$imgs[$i]);			
			}

			$dbOp->insert('article_imgs',$info,$mysqli);		
		}

		if(count($article_audiovisual) > 0){
			$element_type = $article_audiovisual['type'][0];

			$file = save_file($article_audiovisual,"../resources/audiovisuals",'','','',"audiovisual");	
			$info = array();

			/*for ($i=0; $i < count($file) ; $i++) { 
				$info[] = array('',$articles_id,$file[$i],$element_type);			
			}*/

			//workaroumd
			$dbOp->execute_arbitrary("INSERT INTO audiovisual_elements (articles_id,element_src,element_type) VALUES ($articles_id,'{$file[0]}','$element_type') ON DUPLICATE KEY UPDATE  element_src = '{$file[0]}', element_type = '$element_type' ",$mysqli);		
			//print_r($info);
		}
		if ($article_vimeo_video!='') {			

			//echo "<h1>here:$article_vimeo_video</h1>";

			$info=array();
			$info[]= array('',$articles_id,$mysqli->real_escape_string($article_vimeo_video));			
			$dbOp->insert('article_links',$info,$mysqli);

		}

		if ($update_vimeo_video) {
			
			$update_vimeo_video = $update_vimeo_video == '' ? ' ' : $update_vimeo_video;

			$link_id = $dbOp->select("","","","SELECT article_links_id FROM article_links WHERE articles_id = $articles_id",$mysqli);
			$link_id = $link_id[0][0];


			$info = array();
			$info[] = array($link_id,'',$mysqli->real_escape_string($update_vimeo_video));			
			$dbOp->update('article_links',$info,"",$mysqli);			

		}

		if ($article_selected_categories != null) {

			$total_article_selected_categories = count($article_selected_categories);
		
			for ($i = 0; $i < $total_article_selected_categories; $i++) { 				
				$info = array(array("",$articles_id,$article_selected_categories[$i]));	
				$dbOp->insert('articles_blog_categories',$info,$mysqli);																	
			}
			$categories_delete = implode(",", $article_selected_categories);
			$dbOp->delete("articles_blog_categories"," articles_id = $articles_id AND blog_categories_id NOT IN ($categories_delete)",$mysqli);				
    	}

    	do_success_answer("La informacion se ha<br/> actualizado")	;
    	
		?>
		<button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>

		<?php
	}	

	elseif (isset($_POST['new_article_title']) || 
		isset($_POST['new_article_description']) ||
		isset($_POST['new_article_content'])||
		isset($_POST['new_article_creation'])
		 
	){		 
		include ("includes/answers.php");
		$new_article_title = $mysqli->real_escape_string( $_POST['new_article_title']);
		$new_article_description = $mysqli->real_escape_string( $_POST['new_article_description']);
		$new_article_content = $mysqli->real_escape_string( $_POST['new_article_content']);
		$new_article_creation = $mysqli->real_escape_string( $_POST['new_article_creation']);


		
		$info=array();
		$info[]= array('',$new_article_content,$new_article_creation,'',$new_article_description,$new_article_title,'','','','');			

		if($dbOp->insert('articles',$info,$mysqli)){
			do_success_answer("La informacion se ha<br/> actualizado")	;
		}

		else
			do_error_answer("Ha ocurrido un error");

		?>
		<button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>		
		<?php
	}

	elseif (isset($_POST['del_article_id'])) {
		$del_article_id=$_POST['del_article_id'];
							
		$result = $dbOp->delete("articles","articles_id = $del_article_id",$mysqli);
		echo $result ? "se ha eliminado correctamente" : "no se pudo eliminar";		
			
	}

	elseif (isset($_POST['img_id'])) {
		

		$img_id = $mysqli->real_escape_string($_POST['img_id']);
					
		$img = $dbOp->select("","","","SELECT img_src FROM  article_imgs WHERE article_imgs_id = $img_id  ",$mysqli);
		//echo "$img_id";
		$path_delete = "../resources/images/portfolio/".$img[0][0];
		$result = $dbOp->delete("article_imgs","article_imgs_id = $img_id",$mysqli);

		echo $result ? "se ha eliminado correctamente" : "no se pudo eliminar";


		//$path_delete = "../resources/images/portfolio/socks.ph";
		chown($path_delete,1233);// un workaround
		unlink("$path_delete");			
	}


	else{
		include('includes/header.php');	
		include('includes/nav.php');
		
		/*$id = 0;
		$total_rows = 0;

		if(isset($_GET['id']) )
			$id = $_GET['id'];

		$id = $mysqli->real_escape_string(htmlspecialchars($id));	
		$id =  $id >= 0 ? $id : 0;*/
						

		
?>
<div id='main_container' >	
	<div id="blog_article" >

		<h3>Articulos</h3>
		<div id="blog_category" class="blog_categories">
			<form action="blog_category_handler.php" method="post" >
				<table border="0" style="width:20%">
					<tr>
						<th>Categoriy</th>
						<th>Eliminar</th>
					</tr>
				<?php
				$categories = $dbOp->select('' ,'articles',true,"select * from blog_categories",$mysqli );
				
				foreach ($categories as $category ) :																	
			    ?>
					
						<tr>
							<td>
								<input type="text" name="blog_category_entries[]" value="<?php echo $category[1]; ?>" />								
							</td>
							<td>
								<input type="checkbox" name="blog_category_entries_delete[]" value="<?php echo $category[0]; ?>" />
							</td>
							<td>
								<input type="hidden" name="blog_category_entries_ids[]" value="<?php echo $category[0]; ?>" />
							</td>
						</tr>
					
					<?php
					endforeach;
					?>						
				</table>
					<br/>

					<p>Agregar categoria</p>				
						<button id="add_blog_category" class="square"><i class="icon-plus-sign"></i></button>
					<div></div>					

					<br/>
					<br/>
					<button class="medium">Guardar </button>
			</form>
		</div>
		<br/>
		<br/>
		<?php
			$next = 0;
			$previous = 0;			
			$rows = array();

			if (isset($_GET['next'])) {
				$next = intval($_GET['next']);

				$first_article_id = 0;			
				$last_row_id = 0;
				$result = $dbOp->select('' ,'articles',true,"SELECT articles_id FROM articles LIMIT 1",$mysqli );			
				$first_article_id = $next != 0? $next : $result[0][0];			
				$rows = $dbOp->select('' ,'articles',true,"SELECT * FROM articles WHERE articles_id >= $first_article_id LIMIT 3 ",$mysqli );			

				

				
			}

			elseif (isset($_GET['previous'])) {     $previous =
			intval($_GET['previous']);     $last_article_id = 0;
			$last_row_id = 0;     $result = $dbOp->select(''
			,'articles',true,"SELECT articles_id FROM articles ORDER BY
			articles_id DESC LIMIT 1",$mysqli );     $last_article_id =
			$previous != 0? $previous : $result[0][0];     $rows =
			$dbOp->select('' ,'articles',true,"SELECT * FROM articles WHERE
			articles_id <= $last_article_id ORDER BY articles_id DESC LIMIT 3
			",$mysqli );
				
			}
			else{
				$rows = $dbOp->select('' ,'articles',true,"SELECT * FROM articles  LIMIT 3 ",$mysqli );	
				

			}

		
	        foreach ($rows as $key ):	         	         

				$fecha = $newDate = date("d/m/Y", strtotime($key[2]));
				$last_article_id = $article_no = $key[0];

	    ?>
		<div id="article_<?php echo $key[0]?>">
			<form method="post" action="edit_blog.php" enctype="multipart/form-data"> 

				<h5 style="display:inline-table;" >Articulo No. <?php echo $key[0]?></h5><button class="medium red"  style="display:inline-table; margin-left:75px" value="<?php echo $key[0]?>" id="delete_article">Eliminar Elemento </button>
				<br />

				<p>
					<h6>Titulo</h6>
					<textarea class="title" name="article_title" rows="1" cols="70"><?php  echo $key[5] ?></textarea>
				</p>

				<p>
					<h6>Contenido</h6>
					<textarea class="content" name="article_content" rows="1" cols="70"><?php echo $key[1] ?></textarea>
				</p>

				<p>
					<h6>Categoria</h6>
					<ul class="" name="article_category" >
						<?php
						$total_categories = $dbOp->select("","",""," SELECT blog_categories_id FROM articles_blog_categories WHERE articles_id = {$key[0]} ",$mysqli);									

						$checkeds = array();
						foreach ($total_categories as $cat ) {
							$checkeds[] = $cat[0];
						}


						for($i = 0; $i < count($categories); $i++) :																	
			    		?>					
							<li style="display:inline-block;padding-left:20px;">
								<?php echo $categories[$i][1]; ?><input type="checkbox" name="article_selected_categories[]" value="<?php echo $categories[$i][0] ?>" <?php echo in_array($categories[$i][0], $checkeds)? "checked" : ""; ?>/> 

								<input type="hidden" name="article_id" value="<?php echo $key[0]?>" />
							</li>					
						<?php
						endfor;
						?>										
					</ul>
				</p>

				<p>
					<h6>Descripcion</h6>
					<textarea class="description" name="article_description" rows="1" cols="70"><?php echo$key[4] ?></textarea>
				</p>

				<p>
					<h6> Fecha de creacion</h6>
					<input class="creation" type="text" value="<?php echo $fecha ?>" name="article_creation" />
				</p>

				<br />

				<p>					

					<h6>imagenes del articulo</h6>
					<?php							
					$imgs = $dbOp->select('' ,'article_imgs',true," SELECT article_imgs_id,articles_id,img_src FROM article_imgs NATURAL JOIN articles HAVING articles_id = $article_no  ",$mysqli );							
		        	foreach ($imgs as $img ):	         	         

					?>
					
						  <div class="blog_img_container " id="blog_img_<?php echo $img[0]?>" >
            	  			<img src='../resources/images/blog/<?php echo $img[2]?> ' width='200' height='150'/>                              
              				<button class="red" id="delete_blog_img" value="<?php echo $img[0]?>"><i class="icon-minus-sign"></i> borrar</button>
	            		</div>
					<?php
					endforeach;
					?>

					<br/ >
					<p>Agregar imagen</p>
					<button id="blog_article_add_img" class="medium square" style="font-size:150%" value="<?php echo $key[0] ?>"><i class="icon-plus-sign"></i></button>            
					<div id="article_img_<?php echo $key[0] ?>">
					</div>

					<br />
					<br />

					<p>Agregar audio</p>
					<button id="blog_article_add_audio" class="medium square" style="font-size:150%" value="<?php echo $key[0] ?>"><i class="icon-plus-sign"></i></button>            
					<div id="article_audio_<?php echo $key[0] ?>">
					</div>
					
					<br />
					<br />

					<?php
					$link = $dbOp->select('','article_links',true,"SELECT url FROM article_links WHERE articles_id = {$key[0]}",$mysqli);

					

					if(count($link) > 0){

					?>

						<p>Editar video  de <i>Vimeo</i></p>					
						<p><textarea name="update_article_video" rows="1"cols="50"><?php echo str_replace("\\", "", $link[0][0]) ?></textarea></p>
					<?php						

					}
					else{
					?>


					<p>Agregar video  de <i>Vimeo</i></p>					
					<button id="blog_article_add_video" class="medium square" style="font-size:150%" value="<?php echo $key[0] ?>"><i class="icon-plus-sign"></i></button>            					
					<div id="article_video_<?php echo $key[0] ?>">
					</div>

					<?php
					}
					?>

				</p>

				<input type="hidden" value="<?php echo $key[0] ?>" name="articles_id" />

				<button id="save">Guardar</button> 
			 </form> 
        </div> 
        
					 
			<br/><br/>
		<?php 
			endforeach;
		?>

	    <br/><br/><br/>

		<h5>Agregar nuevo articulo</h5>	
		<button id="blog_article_add" class="medium square" style="font-size:150%" ><i class="icon-plus-sign"></i></button>
		<br/><br/>
	</div><!-- blog_article -->
</div><!-- main_container -->



<?php
	$previous_article = $last_article_id;	
	$next_article = $last_article_id;	
	$result = $dbOp->select('' ,'articles',true,"SELECT articles_id FROM articles ORDER BY articles_id DESC LIMIT 1 ",$mysqli );			
	$last_row_id = $result[0][0];
	$result = $dbOp->select('' ,'articles',true,"SELECT articles_id FROM articles ORDER BY articles_id ASC LIMIT 1 ",$mysqli );			
	$first_row_id = $result[0][0];
	


	if($next_article >= $last_row_id) 
		$next_article = 0;
	if($previous_article <= $first_row_id ) 
		$previous_article = $last_row_id;
	/*
	--
	==Paginacion==
	--
	*/

?>
<button id="next_article" class="medium blue " style="text-decoration:none;color:white;font-weight:bold" value="<?php echo $next_article ?>">Siguiente</button>
<button id="previous_article" class="medium blue " style="text-decoration:none;color:white;font-weight:bold" value="<?php echo $previous_article ?>">Anterior</button>



<?php
	}
}
else
    echo "NO ESTA LOGUEADO.PARA HACERLO CLICK <a href='login.php'>AQUI</a>";   

include('includes/footer.php');
?>

