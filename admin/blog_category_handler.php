<?php

//print_r($_POST);

include('includes/edits_header.php');

session_start();

if (isset($_SESSION['user'])){

    include('classes/DatabaseOperations.php');
    include('classes/SimpleImage.php');
    include('includes/upload_file.php');
    include('includes/sanitize_info.php');

    
    $blog_category_entries = isset($_POST["blog_category_entries"])? $_POST["blog_category_entries"] : null ;
    $blog_category_entries_ids = isset($_POST["blog_category_entries_ids"])? $_POST["blog_category_entries_ids"] : null;    
    $new_blog_categories = isset($_POST["new_blog_categories"])? $_POST["new_blog_categories"] : null;
    $blog_category_entries_delete = isset($_POST['blog_category_entries_delete'])? $_POST['blog_category_entries_delete'] : null;
    
    $simple_image = new SimpleImage();
    $dbOp = new DatabaseOperations();
    $mysqli = $dbOp->connection();
    $answer = false;
    
    $info = array();    
    for ($i=0; $i < count($blog_category_entries_ids); $i++) { 

        $info[$i] = array();        
        array_push($info[$i], $blog_category_entries_ids[$i]);
        array_push($info[$i], $blog_category_entries[$i]);        
    }
    
    $info = sanitize_info($info,$mysqli);   
    $answer = $dbOp->update('blog_categories',$info,'',$mysqli);
        
    $info = array();    
    if(count($blog_category_entries_delete) > 0){      

        $blog_category_entries_delete = $mysqli->real_escape_string(implode(",", $blog_category_entries_delete));
        $answer = $dbOp->delete('blog_categories'," blog_categories_id in ($blog_category_entries_delete) ",$mysqli);
    }

 
    $info = array();    
 	if(count($new_blog_categories) > 0){
		$info = array();

    	for ($i=0; $i < count($new_blog_categories) ; $i++) { 
        	$info[$i] = array();
        	array_push($info[$i], '');
            array_push($info[$i], $new_blog_categories[$i]);        
    	} 	
    	$info = sanitize_info($info,$mysqli);   
    	$answer = $dbOp->insert('blog_categories',$info,$mysqli);
 	}

    $answer ? do_success_answer("La informacion <br /> se ha actualizado") : do_error_answer("No se pudo actualizar la informacion");;

    

}

else
    echo "NOT_LOGGED_IN";

include('includes/edits_footer.php');
?>

<button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>
