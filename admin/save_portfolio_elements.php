<?php

include('includes/edits_header.php');

session_start();

if (isset($_SESSION['user'])){

	include('classes/DatabaseOperations.php');
	include('classes/SimpleImage.php');
	include('includes/upload_file2.php');
	include('includes/sanitize_info.php');


	$dbOp = new DatabaseOperations();
	$mysqli = $dbOp->connection();
	$dir = "../resources/images/portfolio";
	$simple_image = new SimpleImage();	



	$id = $_POST['portfolio_item_id'];
	$filter = $_POST['portfolio_item_filter'];			
	$main_img =  isset($_FILES['portfolio_item_main_img'])? $_FILES['portfolio_item_main_img'] : null ;	
	$title = $_POST['portfolio_item_title'];		
	$pararaph = $_POST['item_paragraph'];
	$imgs = isset($_FILES['portfolio_elements_img'])?$_FILES['portfolio_elements_img'] : null;	

	$all_imgs = array();

	if(strlen($main_img['name'][0]) > 0){
		$main_tmp_name = $_FILES['portfolio_item_main_img']['tmp_name'][0];

		$size = getimagesize($main_tmp_name);			
		$all_imgs[] = array("img" => $main_img, "size" => $size);
		$main_img = array($main_img['name'][0]);
	}

	 else
	 	$main_img = array('');

	$info = array();
	for ($i = 0; $i < count($pararaph); $i++) { 
    	$info[$i] = array();
    	array_push($info[$i], $id);
		array_push($info[$i], $filter);
		array_push($info[$i], $main_img[$i]);
		array_push($info[$i], '');
		array_push($info[$i], $title);
		array_push($info[$i], $pararaph);
	}

	$info = sanitize_info($info,$mysqli);		
	$dbOp->update('portfolio_items',$info,'',$mysqli);


	if( strlen($imgs['name'][0]) > 0){	
		$new_img_tmp = $imgs['tmp_name'][0];
		$size = getimagesize($new_img_tmp);

		$all_imgs[] = array("img" => $imgs, "size" => $size);

		$info = array();

		for ($i = 0; $i < count($imgs['name']); $i++) { 
	    	$info[$i] = array();
	    	array_push($info[$i], '');
			array_push($info[$i], $id);
			array_push($info[$i], $imgs['name'][$i]);
		}		

		$dbOp->insert('portfolio_single_item_imgs',$info,$mysqli);
	}
	$messages = array('msgs' => array(), 'results' => null);
	foreach ($all_imgs as $index => $img) {
		$messages = save_file($all_imgs[$index]['img'], $dir, $all_imgs[$index]['size'][0], $all_imgs[$index]['size'][1], $simple_image, 'img');	
	}
	
		
	do_upload_answers($messages['msgs']);
	
	
}

	
else
	echo "NOT_LOGGED_IN";

include('includes/edits_footer.php');
?>

<button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>