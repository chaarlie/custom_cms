<?php

session_start();

include('classes/DatabaseOperations.php');
include('includes/answers.php');
include('includes/edits_header.php');

if (isset($_SESSION['user'])){

	$dbOp = new DatabaseOperations();
	$mysqli = $dbOp->connection();

	if(isset($_POST['username']) && 
		isset($_POST['password1']) &&
		isset($_POST['password2'])
	){		
		$username = $mysqli->real_escape_string(htmlspecialchars($_POST['username']));
		$password1 = $mysqli->real_escape_string(htmlspecialchars($_POST['password1'] ));
		$password2 = $mysqli->real_escape_string(htmlspecialchars($_POST['password2'] ));

		if($password1 === $password2){

			$user = $dbOp->select('' ,'','',"SELECT username FROM users WHERE username ='$username'",$mysqli );			
			
			if (isset($user[0][0])) {
				$user = $user[0][0];
			
				do_error_answer( "existe usuario<br/>$user");		
			}
			else{
				$password = md5($password1);
				$username  = strtolower($username);
				$values = array(array('',$username,$password));
				
				$dbOp->insert('users' ,$values,$mysqli );
				do_success_answer("SE HA CREADO <br /> $username");
			}

		}
	}
}

else{
    echo "NO ESTA LOGUEADO.PARA HACERLO CLICK <a href='login.php'>AQUI</a>";    
}
?>
<button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>