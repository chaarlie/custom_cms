
<?php

function do_upload_answers($msgs=array()){
	echo <<<_END
	<div id='img_successful_answer_container'>
		<div>
			<h3>LA INFORMACION <br />SE HA PROCESADO</h3>
		</div>
_END;

	if( count($msgs) > 0){
		echo <<<_END
			
			<div id='img_upload_answer' class='notice success'>
_END;
		foreach ($msgs as $current => $msg) 
			echo "<p class='icon-ok icon-large'>{$msg} </p><br />";
		
		echo "</div>";
}	
echo "</div>";
}


function do_success_answer($answer){
	echo <<<_END
	<div id='successful_operation'>
		<div>
			<h3>$answer</h3>
		</div>		
		<br />
_END;

	echo "</div>"	;
}

function do_error_answer($answer){
	echo <<<_END
	<div id='failed_operation'>					
		<div id="error_answer" class="notice error"><i class="icon-remove-sign icon-large"></i><div>$answer</div></div>
	</div>
_END;
}

//do_error_answer("lol");

?>
