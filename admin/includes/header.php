<!DOCTYPE html>
<html>
<head>
	<title>Zeros Panel</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="js/jquery.js"></script>
	<link rel="stylesheet" type="text/css" href="css/kickstart.css" media="all" />  
	<link href='http://fonts.googleapis.com/css?family=Fauna+One' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Prosto+One' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
	<script src="http://malsup.github.com/jquery.form.js"></script> 
	<script type="text/javascript" src="js/custom_js.js"></script>
	<script src="js/kickstart.js"></script> 
	
	<!--[if IE 8]>
	<style type="text/css">		
	div#wrong_password, div#password_pattern{
		filter: alpha(opacity = 0);
	}
	</style>
	<![endif]-->

	<!--[if lt IE 8]>
	<style type="text/css">
	div#wrong_password, div#password_pattern{
		 filter: alpha(opacity = 0);
	}			
	</style>
	<![endif]-->
	
</head>
<body>