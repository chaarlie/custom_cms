<?php
function sanitize_info($info,$mysqli){
		for ($i=0; $i < count($info); $i++) { 
			for ($j=0; $j < count($info[$i]); $j++) { 
				if(is_string($info[$i][$j]))
					$info[$i][$j] = $mysqli->real_escape_string( htmlspecialchars($info[$i][$j]) );
			}
		}
		return $info;
	}
?>