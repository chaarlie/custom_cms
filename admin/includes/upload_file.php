<?php
include('includes/answers.php');

function save_file($files,$dir,$width,$height,$simple_image,$type){

    //print_r($files);
    $msgs = array();

    $img = array('image/gif', 'image/jpeg', 'image/pjpeg','image/png');
    $video =array('application/ogg','audio/mpeg','audio/mp4','audio/ogg','audio/wav', 'video/ogg', 'video/mp4','video/x-m4v','audio/mp4');

    
    $results = array();

    $file_index = 0;

    switch ($type) {
        case 'img':
            $permitted = $img;
            break;
        case 'audiovisual':
            $permitted = $video;
            break;                           
    }

    foreach ($files['name'] as $key=>$element) { 

        $file_type = $files['type'][$key];
        $file_name = $files['name'][$key];
        $file_size = $files['size'][$key];
        $file_error = $files['error'][$key];
        $file_tmp_name = $files['tmp_name'][$key];

      
            /*echo "<h1>".$file_name."</h1>";
            echo "<h1>".$file_type."</h1>";
*/
        $file_name = str_replace(' ', '_', $file_name);

        $success = null;
        if($file_type!==''){
            if (in_array($file_type, $permitted)){
                switch ($file_error) {
                    case 0:
                        $file = "$dir/$file_name";

                        $success = move_uploaded_file($file_tmp_name,$file);  

                        if($success && $type == 'img'){
                            $simple_image->load($file);
                            $simple_image->resize($width,$height);
                            $simple_image->save($file);
                        }

                        //echo $success?"$file_name se ha subido con exito.":"Error al subir $file_name";
                        $msgs[] = $success? "$file_name se ha subido con exito.":"Error al subir $file_name";
                        break;
            
                    case 3:
                    case 6:
                    case 7:
                    case 8:
                        $msgs[] =  "Error no. 8 . Por favor intentar denuevo";
                    break;
                    case 4:
                        $msgs[] =  "No se selecciono un archivo.";
                }
            } 
            else    
                $msgs[] = isset($file_name)? "$file_name no es un archivo valido.":"no selecciono ningun archivo.";        
        }

        $results[$file_index] = $file_name;
        $file_index++;    
    }
     
    
    if(count($msgs) > 0){
        do_upload_answers($msgs);
        
    }
    else{
            do_upload_answers();
            //do_error_answer("No se encontraron imagenes o texto ");
    }
    return $results;
}


?>