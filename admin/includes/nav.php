<?php
if (isset($_SESSION['user'])){	
	
?>
<header>
	<nav>
		<ul class='menu'>
			<li class="divider"><a href="index.php">Inicio</a></li>
			<li class="divider"><a href="create_account.php">Crear Cuenta</a></li>
			<li class="divider"><a href="#">Editar Contenido</a>
				<ul>
					<li class="divider"><a href="edit_index.php">Home</a></li>
					<li class="divider"><a href="edit_about.php">Acerca</a></li>					
					<li class="divider"><a href="edit_portfolio.php">Folio</a></li>
					<li class="divider"><a href="edit_blog.php">Blog</a></li>
					<li class="divider"><a href="edit_contact.php">Contactenos</a></li>
				</ul>
			</li>
		</ul>
	</nav>
</header>
<?php
}
else{
	echo "<div class='not_logged'><p>No ha iniciado session<br/><span>puede hacerlo <a herf='login.php'>aqui</a></p></span></div>";
}
?>