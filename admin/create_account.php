	<?php
	session_start();

	include('includes/header.php');

	if (isset($_SESSION['user'])){

		include('classes/DatabaseOperations.php');
		include('includes/nav.php');

		$dbOp = new DatabaseOperations();
		$mysqli = $dbOp->connection();

		$user = $_SESSION['user'];

		?>
		
		<div id='main_container'>
			<button class="medium blue pill" id="user_logout" >Desloguearse</button>

			         

			<h3>Formulario de registro</h3>

			<div id="password_pattern_container">
		        <button id="password_pattern_button" >Patron de Contraseña</button>
		        <div id="password_pattern" style="opacity:0">        
		        </div>
		    </div>	           

		    <div id="wrong_password" class="notice error">
	    		
			</div>   
		                
			
			<div id="create_account" class="input">
				<form action="create_account_handler.php" method="post">
					<ul>
						<li>usuario:<input type="text" id="username" name="username" /></li>
						<li>contraseña:<input type="password" name="password1" id="password1" /></li>
						<li>repetir contraseña:<input type="password" name="password2" id="password2" /></li>
						<li><button class="small blue " id="create_account_button"> Crear</button></li>
					</ul>
					
				</form>
			</div>
		</div>
	<?php
	}
	else
		echo "NO ESTA LOGUEADO.PARA HACERLO CLICK <a href='login.php'>AQUI</a>";

	include('includes/footer.php');
	?>
