<?php
session_start();

include('includes/header.php');

if (isset($_SESSION['user'])){

	include('classes/DatabaseOperations.php');
	include('includes/nav.php');

	$dbOp = new DatabaseOperations();
	$mysqli = $dbOp->connection();

	$user = $_SESSION['user'];

	?>
	
	<div id='main_container'>
		<button class="medium blue pill" id="user_logout" >Desloguearse</button>
		<form action="edit_contact_handler.php" method="post" >
		<h3>Contacto</h3>

		<p>
			direccion<br/>
	        <?php
	         $result = $dbOp->select('*' ,'contact_infos',true,'',$mysqli );
	        ?>
	  
	        <textarea name="contact_address"><?php echo $result[0][1]; ?></textarea>
        </p>
	    <p>
	    	Telefono:
	        <input type="text" name="contact_tel" value="<?php echo $result[0][2]; ?>"> 
	    </p>
	    <p>
	        Website: <input type="text" name="contact_site" value="<?php echo $result[0][3]; ?>" size="25"/>
	    </p>
	    <p>
	        <?php
	         $mail_types = array("Email principal","Email secundario");	
	         $i = 0;
	         $result = $dbOp->select('' ,'',true,' SELECT * FROM contact_info_emails WHERE contact_info_emails.contact_infos_id = 1 ',$mysqli );
	         foreach ($result as $key ) {  
	        ?>
	        <?php 
	        $len = strlen($mail_types[$i]);
	        echo $mail_types[$i];
	        $i++; 


	        ?>: <input type="text" name="contact_emails[]" value="<?php echo $key[2]?>" size="<?php echo $len+20 ?>"> <br/>
    	</p>
                <?php
                }
                ?>	
        <br/>
    	<br/>
    	<button class="medium blue " id="submit">Guardar</button>
    	<br/>
    	<br/>
    	<br/>
   	</form>
	</div>
<?php
}
else
	echo "NO ESTA LOGUEADO.PARA HACERLO CLICK <a href='login.php'>AQUI</a>";

include('includes/footer.php');
?>
