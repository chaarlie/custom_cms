<?php

session_start();

include('classes/DatabaseOperations.php');


if (isset($_SESSION['user'])){



$dbOp = new DatabaseOperations();
$mysqli = $dbOp->connection();
    

    if(isset($_FILES["index_banners_imgs"]) || 
        isset($_POST["index_banners_titles"])||
        isset($_POST["index_banners_texts"])||
        isset($_POST["index_banners_ids"])

    ){

        include('includes/edits_header.php');    
        include('classes/SimpleImage.php');
        include('includes/upload_file.php');
        include('includes/sanitize_info.php');

        $index_banners_imgs = $_FILES["index_banners_imgs"];
        $index_banners_titles = $_POST["index_banners_titles"];
        $index_banners_texts = $_POST["index_banners_texts"];
        $index_banners_ids = $_POST["index_banners_ids"];

        $simple_image = new SimpleImage();
        
        $dir = '../resources/images/';
        $img_sources = save_file($index_banners_imgs,$dir,1060,350,$simple_image,'img' );
        $info = array();

        for ($i=0; $i < count($index_banners_ids) ; $i++) { 
            $info[$i]=array();
            array_push($info[$i], $mysqli->real_escape_string(htmlspecialchars ($index_banners_ids[$i])));
            array_push($info[$i], $mysqli->real_escape_string(htmlspecialchars ($index_banners_titles[$i])));
            array_push($info[$i], $mysqli->real_escape_string(htmlspecialchars ($img_sources[$i])));
            array_push($info[$i], $mysqli->real_escape_string(htmlspecialchars ($index_banners_texts[$i])));
        }

        $info = sanitize_info($info,$mysqli);   

        $result = $dbOp->update('index_banners',$info,'',$mysqli);

        ?>
        <button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>
        <?php
    }

    elseif (isset($_POST['banner_delete_id'])) {
        $banner_delete_id=$_POST['banner_delete_id'];
                            
        $result = $dbOp->delete("index_banners","index_banners_id = $banner_delete_id",$mysqli);
        echo $result ? "se ha eliminado correctamente" : "no se pudo eliminar";     
            
    }


    elseif (isset($_FILES['index_top_banner'])||
            isset($_POST['index_top_description'])||
            isset($_POST['index_top_title'])
    ) {


        include('includes/edits_header.php');
        include('classes/SimpleImage.php');
        include('includes/upload_file.php');

        $simple_image = new SimpleImage();

        $description = $mysqli->real_escape_string(htmlspecialchars ($_POST['index_top_description']));     
        $title = $mysqli->real_escape_string(htmlspecialchars ($_POST['index_top_title']));
        $banner = $_FILES['index_top_banner'];

        $dir = '../resources/images/';
        $banner = save_file($banner,$dir,1060,350,$simple_image,'img' );    

        $info = array(array("",$title,$banner[0],$description));        
        $result = $dbOp->insert("index_banners",$info,$mysqli);
            
     ?>
        <button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>
        <?php
    }

    else {
        include('includes/header.php');
        include('includes/nav.php');
    ?>
        <div id='main_container'>
        <form action='edit_index.php' method='post' enctype="multipart/form-data" id='index'>
            <div id="index_top_banners">
                <button class="medium blue pill" id="user_logout" >Desloguearse</button>
                <h3>Banners Principales</h3>
                <?php
                $result = $dbOp->select('*' ,'index_banners',true,'',$mysqli );
                foreach ($result as $key ) {  ?>
                    <div id="index_banner_<?php echo $key[0];?>">
                        <img src="../resources/images/<?php echo "{$key[2]}"; ?>" width="250" height="200"/>
                        <input type="file" name="index_banners_imgs[]"  />
                        <p>Titulo:<br />
                            <input type="text" name="index_banners_titles[]" value="<?php echo $key[1]; ?>" />
                        </p>
                        <p>Contenido:<br />
                            <textarea name="index_banners_texts[]"><?php echo $key[3]; ?> </textarea>
                        </p>
                        <input type="hidden" name="index_banners_ids[]" value="<?php echo "{$key[0]}"; ?>"/>            
                    
                        <button class='index_banners_delete' value="<?php echo $key[0];?>">Eliminar</button>

                        <br />
                        <br />
                        <br />
                    </div>

                <?php
                }
                ?>
                
                <br />
                <br />
                <br />

                <button id="index_top_banners_add" class="medium square" style="font-size:150%" ><i class="icon-plus-sign"></i></button>
            </div>


            <button class="medium blue " id="submit">Guardar</button>
            <br />
            <br />
            
            
        </form>
        </div>

        <?php
        include('includes/footer.php');
    }
}

else{
    echo "NO ESTA LOGUEADO.PARA HACERLO CLICK <a href='login.php'>AQUI</a>";
    include('includes/footer.php');
}
?>


