<?php

include('includes/edits_header.php');

session_start();

if (isset($_SESSION['user'])){

	include('classes/DatabaseOperations.php');
	include('classes/SimpleImage.php');
	include('includes/upload_file.php');
	include('includes/sanitize_info.php');


	$dbOp = new DatabaseOperations();
	$mysqli = $dbOp->connection();
	$simple_image = new SimpleImage();

	$about_main_img = $_FILES['about_main_img'];
	$about_main_img_id = $_POST['about_main_img_id'];
	$about_main_paragraphs = $_POST['about_main_paragraphs'];
	$about_main_paragraphs_ids = $_POST['about_main_paragraphs_ids'];


	$about_skill_values = $_POST['about_skill_values'];
	$about_skill_names = $_POST['about_skill_names'];	
	$about_skill_ids = $_POST['about_skill_ids'];

	$about_team_ids = $_POST['about_team_ids'];
	$about_team_imgs = $_FILES['about_team_imgs'];
	$about_team_names = $_POST['about_team_names'];
	$about_team_positions = $_POST['about_team_positions'];
	$about_team_descriptions = $_POST['about_team_descriptions'];


	$about_team_new_imgs=isset($_FILES['about_team_new_imgs']) ? $_FILES['about_team_new_imgs'] : null;
	$about_team_new_names=isset($_POST['about_team_new_names']) ? $_POST['about_team_new_names'] : null;
	$about_team_new_positions=isset($_POST['about_team_new_positions']) ? $_POST['about_team_new_positions'] : null;
	$about_team_new_descriptions=isset($_POST['about_team_new_descriptions']) ? $_POST['about_team_new_descriptions'] : null;

	$about_skills_new_names = isset($_POST['about_skills_new_names']) ? $_POST['about_skills_new_names'] : null;
	$about_skills_new_values = isset($_POST['about_skills_new_values']) ? $_POST['about_skills_new_values'] : null;
	
	$all_images = array();

	/*
	about_main_paragraphs
	*/

	$info = array();

	for ($i = 0; $i < 1; $i++) { 
	    $info[$i] = array();
    	array_push($info[$i], $about_main_paragraphs_ids[$i]);
    	array_push($info[$i], '');
    	array_push($info[$i], $about_main_paragraphs[$i]);    	
	}

	$info = sanitize_info($info,$mysqli);	
	$dbOp->update('about_main_paragraphs',$info,'',$mysqli);
	


	/*
	about_main_img
	*/

	$info = array();

	$dir = "../resources/images/about";


	if(count($about_main_img['name']) > 0){
		$all_images = array_merge($all_images,$about_main_img);
	
		for ($i = 0; $i < 1; $i++) { 
		    $info[$i] = array();
	    	array_push($info[$i], $about_main_img_id[$i]);
	    	array_push($info[$i], $about_main_img['name'][$i]);    	
		}

		$info = sanitize_info($info,$mysqli);	
		$dbOp->update('about_main_contents',$info,'',$mysqli);
	}

	
	
	/*
	about_skills
	*/

	$info = array();

	for ($i = 0; $i < count($about_skill_ids); $i++) { 
	    $info[$i] = array();
    	array_push($info[$i], $about_skill_ids[$i]);
    	array_push($info[$i], $about_skill_names[$i]);
    	array_push($info[$i], $about_skill_values[$i]);    	
	}

	$info = sanitize_info($info,$mysqli);	
	$dbOp->update('about_skills',$info,'',$mysqli);

	
	

	/*
	about_teams
	*/

	
	$info = array();
	$dir = "../resources/images/about";
	if(count($about_team_imgs['name']) > 0){
		$all_images = array_merge($all_images, $about_team_imgs);

	}	

	for ($i = 0; $i < count($about_team_ids); $i++) { 
	    $info[$i] = array();
    	array_push($info[$i], $about_team_ids[$i]);    	
    	
    	if(count($about_team_imgs['name']) 	== 0)
    		array_push($info[$i], '');
    	
    	else
    		array_push($info[$i], $about_team_imgs['name'][$i]);

    	array_push($info[$i], $about_team_names[$i]);    	
    	array_push($info[$i], $about_team_positions[$i]);    	
    	array_push($info[$i], $about_team_descriptions[$i]);    	
	}

	$info = sanitize_info($info,$mysqli);	
	$dbOp->update('about_team_members',$info,'',$mysqli);	


	/*
	about_team_members new
	*/


	
	$dir = "../resources/images/about";
	if(count($about_team_new_imgs['name']) > 0)
		$all_images = array_merge($all_images, $about_team_new_imgs);

	if ($about_team_new_names != null) {
		
	
		$info = array();
		for ($i = 0; $i < count($about_team_new_names); $i++) { 
		    $info[$i] = array();
	    	array_push($info[$i], '');
	    	array_push($info[$i], $about_team_new_imgs['name'][$i]);    	
	    	array_push($info[$i], $about_team_new_names[$i]);    	
	    	array_push($info[$i], $about_team_new_positions[$i]);    	
	    	array_push($info[$i], $about_team_new_descriptions[$i]);    	
		}


		$info = sanitize_info($info,$mysqli);	
	
		$dbOp->insert('about_team_members',$info,$mysqli);	//workaround

		
	}

	/*
	about_skills new
	*/

	if ($about_skills_new_names != null) {
		
		$info = array();	
		for ($i = 0; $i < count($about_skills_new_names); $i++) { 
		    $info[$i] = array();
	    	array_push($info[$i], '');
	    	array_push($info[$i], $about_skills_new_names[$i]);    	
	    	array_push($info[$i], $about_skills_new_values[$i]);    	
	    	
		}

		$info = sanitize_info($info,$mysqli);		
		$dbOp->insert('about_skills',$info,$mysqli);
		
	}

	$all_images= save_file($all_images,$dir,240,300,$simple_image,'img');

}

else
	echo "NOT_LOGGED_IN";

include('includes/edits_footer.php');
?>

<button class="return" style="text-decoration:none;color:grey;font-weight:bold">Regresar</button>