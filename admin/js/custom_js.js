$(function(){

	/*
	*******
	ARCHIVO: includes/nav.php
	*******
	*/
	var currAddress =  document.location.pathname;
	//currAddress = currAddress.substring(currAddress.length - 9, currAddress.length);
	$("ul.menu li a").each(function(){
		if(currAddress.endsWith($(this).attr("href"))){
			$("ul.menu li").removeClass("current");
			$(this).addClass("current");
		}
		
	});


	/*
	*******
	ARCHIVO: includes/nav.php
	*******
	*/




	/*
	*******
	ARCHIVO: create_account.php
	*******
	*/


	$("div#create_account input#password2").focusout(function(){
		var password1 = $("div#create_account input#password1");
		var password2 = $("div#create_account input#password2");

		var testRegx = /^(?=.*[0-9])(?=.*[!@#$%^&*-_])[a-zA-Z0-9!@#$%^&*-_]{6,16}$/;

		if(password1.val() !== '' && password2.val() !== ''){
			if(password1.val() !== password2.val()){
				$("div#wrong_password ").text("Las constraseñas no son iguales");

				$("div#wrong_password").animate({opacity:1.0},3000,function(){      						
					password1.val("");
					password2.val("");

				}).animate({opacity:0}, 900);	      					
			}
			
			if(!testRegx.exec(password1.val()) ){
				$("div#wrong_password ").text("La constraseña no cumple con el patron");

				$("div#wrong_password").animate({opacity:1.0},3000,function(){      						
					password1.val("");
					password2.val("");
				}).animate({opacity:0},900);	    
			}      				
		} 
	});



	$("div#create_account input#password2").focusout(function(){
		var password1 = $("div#create_account input#password1");
		var password2 = $("div#create_account input#password2");
		var testRegx = /^(?=.*[0-9])(?=.*[!@#$%^&*-_])[a-zA-Z0-9!@#$%^&*-_]{6,16}$/;

		if(password1.val() !== '' && password2.val() !== ''){
			if(password1.val() !== password2.val()){
				$("div#wrong_password ").text("Las constraseñas no son iguales");
				$("div#wrong_password").animate({opacity:1.0}, 3000, function(){      						
					password1.val("");
					password2.val("");
				}).animate({opacity:0}, 900);	      					
			}

			if(!testRegx.exec(password1.val()) ){
				$("div#wrong_password ").text("La constraseña no cumple con el patron");
				$("div#wrong_password").animate({opacity:1.0}, 3000, function(){      						
					password1.val("");
					password2.val("");
				}).animate({opacity:0}, 900);	    
			}      				
		} 
	});


	$("button#password_pattern_button").click(function(event){
		event.preventDefault();

		$("div#password_pattern").append(	 	      
			'<ul class="alt">'+
			'<li>Solo letras de la a a la z mayusculas o minusculas</li>'+
			'<li>Por lo menos 1 numero del 0 al 9</li>'+
			'<li>Puede tener:!@#$%^&*-_</li>'+
			'<li>Tiene que tener entre 6 y 16 caracteres</li>'+
			'</ul>'
			).animate({opacity:1}, 900);	    

		$(this).click(function(event){
			event.preventDefault();

			$("div#password_pattern").hide();
		});
	});	


	/*
	*******
	ARCHIVO: create_account.php
	*******
	*/

	$("button#password_pattern_button").click(function(event){
		event.preventDefault();

		$("div#password_pattern").append(
			/* '<h5>La contraseña para ser valida debe cumplir las siguientes reglas:</h5>'+*/
			'<ul class="alt">'+
			'<li>Solo letras de la a a la z mayusculas o minusculas</li>'+
			'<li>Por lo menos 1 numero del 0 al 9</li>'+
			'<li>Puede tener:!@#$%^&*-_</li>'+
			'<li>Tiene que tener entre 6 y 16 caracteres</li>'+
			'</ul>'
			).animate({opacity:1},900,function(){});	    

		$(this).click(function(event){
			event.preventDefault();

			$("div#password_pattern").hide();
		});

	});

    	/*
		edit_about.php		
		*/

		$("button#about_skill_add").click(function(event){
			event.preventDefault();
			var options = "";
			for (var i = 1; i <101; i++) 
				options += "<option>" + i + "</option>";
			$("div#about_skills_container").append("<div style='opacity:0' class='about_skill' ><p>nombre:<br /><input type='text' name='about_skills_new_names[]'/></p><p>valor:</br><select name='about_skills_new_values[]'>" + options + "</select></p><br/><br/></div>");
			$("div.about_skill").animate({opacity:1.0},900);
		});

		$("button#about_members_add").click(function(event){
			event.preventDefault();
			$(this).before(
				'<div style="opacity:0" class="about_member"><br/>'+
				'<p  >'+
				'<input  type="file" name="about_team_new_imgs[]" />'+
				'</p>'+
				'<p >'+
				'Nombre:<br/>'+
				'<input type="text" name="about_team_new_names[]" />'+
				'</p>'+
				'<p  >'+
				'Posicion:<br/>'+
				'<input type="text" name="about_team_new_positions[]" />'+
				'</p>'+
				'<p  >'+	
				'Descripcion:<br/>'+
				'<textarea rows="3" cols="33" name="about_team_new_descriptions[]"></textarea>'+
				'</p>'+
				'</div>'
				);

			$("div.about_member").animate({opacity:1.0},900);
            	//alert($("div.my_div").html());
            });

		$("button.about_members_delete").click(function(event){
			event.preventDefault();
			response = confirm("Desea realmente eliminar el elemento? ");
			var id = $(this).attr("value");
			if(response == true){
				$("div#about_members_" + id).animate({opacity:0.0,height:0},900,function(){
					$(this).remove();
				});
				$.post("delete_member.php",{id:id},function(data){
						//alert(data);
					});
			}
		});

		$("button#about_skill_delete").click(function(event){
			event.preventDefault();

			var skill = "div#skill_"+$(this).attr("value");
			var id = $(this).attr("value");

			response = confirm("Desea realmente eliminar el elemento? ");
			if(response == true){
				$.post("delete_skills.php",{id:id},function(data){
//						alert(data);
});

				$(skill).animate({opacity:0.0,height:0},900,function(){
					$(this).remove();
				});
			}
		});

		$("button#portfolio_elements_paragraph_add").click(function(event){
			event.preventDefault();
			var id = $(this).attr("value");


				/*$("div#paragraphs_element_" + id).append(
					"<p>parrafo:</p>"+
					"<textarea rows='3' cols='30' name='portfolio_elements_paragraph[]' class='portfolio_elements_paragraph" + id + "' ></textarea>" +
					"<br />"
				);
		$("div#paragraphs_element_" + id).animate({opacity:1},900,function(){});*/
	});

		$("button#portfolio_elements_img_add").click(function(event){
			event.preventDefault();
			var id = $(this).attr("value");

			$("div#imgs_element_" + id).append(
				"<p>imagen:</p>"+
					//"<input type='file' name='portfolio_elements_img" + id + "[]'/>" +
					"<input type='file' name='portfolio_elements_img[]' class='portfolio_elements_img" + id + "'/>" +
					"<br />"
					);
			$("div#imgs_element_" + id).animate({opacity:1},900,function(){});
		});

		$("button.save_button").click(function(event){			

				//event.preventDefault();								
			}); 


		$("button#delete_portfolio_img").click(function(event){
			event.preventDefault();
			var id = $(this).attr("value");
			var img_div = $("div.portfolio_img_" + id);

			response = confirm("Desea realmente eliminar el elemento? ");
			if(response == true){
				$.post("delete_portfolio_img.php",{id:id},function(data){
//						alert(data);
});

				img_div.animate({opacity:0.0,height:0},900,function(){
					$(this).remove();
				});					
			}		
		});

		$("button#add_new_portfolio_element").click(function(event){
			event.preventDefault();				
			var new_portfolio_element_div = $("div#new_portfolio_element" );

			new_portfolio_element_div.animate({opacity:1},900,function(){})       						
		});

		$("button#user_logout").click(function(event){
			event.preventDefault();
			window.location = "logout.php";
		});


			 /*
			 	index.php



			 	*/

			 	$("button#change_password").click(function(event){

			 		var username = $(this).attr("value");			 	
			 	//alert(username);
			 	event.preventDefault();
			 	$("div#password_form").append(
			 		'<div class="input" id="change_password" style="opactiy:0">'+
			 		'<form action="change_password.php" method="post">'+
			 		'<ul>'+
			 		'<li>contrasena actual:<input type="password" name="old_password" /></li>'+
			 		'<li>contrasena nueva:<input id="new_password1" type="password" name="new_password1" /></li>'+
			 		'<li>repetir contrasena nueva:<input id="new_password2" type="password" name="new_password2"/></li>'+
			 		'<input type="hidden" name="username" value="'+ username +'"/>'+
			 		'<li><button class="small blue"  value="' + $(this).attr("value") + '">Cambiar</button></li>'+					
			 		'</ul>'+					
			 		'</form>'+
			 		'</div> '
			 		).animate({opacity:1.0},900,function(){});	

			 	$("div#main_container input#new_password2").focusout(function(){					
			 		var password1 = $("div#main_container input#new_password1");
			 		var password2 = $("div#main_container input#new_password2");

			 		var testRegx = /^(?=.*[0-9])(?=.*[!@#$%^&*-_])[a-zA-Z0-9!@#$%^&*-_]{6,16}$/;

			 		if(password1.val() !== '' && password2.val() !== ''){
			 			if(password1.val() !== password2.val()){
			 				$("div#wrong_password ").text("Las constraseñas no son iguales");

			 				$("div#wrong_password").animate({opacity:1.0},3000,function(){      						
			 					password1.val("");
			 					password2.val("");

			 				}).animate({opacity:0},900,function(){
	      						//`$(this).remove();
	      					});	      					
			 			}
			 			else
			 				if(! testRegx.exec(password1.val()) ){
			 					$("div#wrong_password ").text("La constraseña no cumple con el patron");

			 					$("div#wrong_password").animate({opacity:1.0},3000,function(){      						
			 						password1.val("");
			 						password2.val("");

			 					}).animate({opacity:0},900,function(){
	      							//`$(this).remove();
	      						});	    
			 				}
			 			}      				
			 		});				 					 

		$(this).remove();

});  






	/*
	edit_index.php
	*/


	$("button#index_top_banners_add").click(function(event){			 				 

		event.preventDefault();
		$("div#index_top_banners").append(

			'<form action="edit_index.php" method="post" enctype="multipart/form-data">'+
			'<p>Nueva imagen:<input type="file" name="index_top_banner[]" /></p>'+				 	
			'<p>Titulo:<input  type="text" name="index_top_title" /></p>'+
			'<p>Descripcion:<br/><textarea rows="4" cols="22" name="index_top_description"></textarea></p>'+					
			'<p><button class="small blue"  value="' + ''+ '">Guardar</button></p>'+					
			'</form>'

			).animate({opacity:1.0},900,function(){});
	});	

	$("button.index_banners_delete").click(function(event){
		event.preventDefault();
		response = confirm("Desea realmente eliminar el elemento? ");
		var id = $(this).attr("value");
		if(response == true){
			$("div#index_banner_" + id).animate({opacity:0.0,height:0},900,function(){
				$(this).remove();
			});
			$.post("edit_index.php",{banner_delete_id:id},function(data){
			//alert(data);
		});
		}
	});



 /*
	edit_blog.php
	*/			  

	$("button#blog_article_add").click(function(event){
		event.preventDefault();
		$(this).remove();

		$("div#blog_article").append(
			'<form action="" method="post" enctype="multipart/form-data">'+				 					 	
			'<p>Titulo:<br/><textarea class="title" name="new_article_title" rows="1" cols="70"></textarea></p><br />'+				 	
			'<p>Descripcion:<br/><textarea class="description" name="new_article_description" rows="13" cols="90"></textarea></p><br />'+				 	
			'<p>Contenido:<br/><textarea class="content" name="new_article_content" rows="3" cols="90"></textarea></p><br />'+
			'<p>fecha: <input name="new_article_creation" type="text" /></p><br />'+						
			'</p><br />'+						
			'<button class="medium blue " id="submit">Guardar</button>'+
			'</form><br /><br />'

			).animate({opacity:1.0},900,function(){});

		$("button#blog_img_add").click(function(event){
			event.preventDefault();

			$("div.blog_img").append(
				'<p>imagen<input name="article_imgs[]" type="file" /></p>'
				).animate({opacity:1.0},900,function(){});	            	          

		});
		$("button#blog_element_add").click(function(event){
			event.preventDefault();

			$("div.blog_element").append(
				'<p>imagen<input type="file" /></p>'
				).animate({opacity:1.0},900,function(){});
		});	


	});

	$("button#next_article").click(function(event){
	event.preventDefault();
	var id = $(this).attr('value');
	window.location="edit_blog.php?next="+id;

	});

	$("button#previous_article").click(function(event){
	event.preventDefault();
	var id = $(this).attr('value');
	window.location="edit_blog.php?previous="+id;

	});


	$("button#blog_article_add_img").click(function(event){
	event.preventDefault();
	var id = $(this).attr('value');
	$("div#article_img_"+id).append(
		'<p><input type="file" name="appent_article_imgs[]" /></p>'
		).animate({opacity:1.0},900,function(){});            		

	});


	$("button#blog_article_add_audio").click(function(event){
	event.preventDefault();
	var id = $(this).attr('value');
	$(this).remove();
	$("div#article_audio_"+id).append(
		'<p><input type="file" name="appent_article_audiovisual[]" /></p>'
		).animate({opacity:1.0},900,function(){});            	
	});

	$("button#blog_article_add_video").click(function(event){
	event.preventDefault();
	var id = $(this).attr('value');
	$(this).remove();
	$("div#article_video_"+id).append(
		'<p><textarea name="appent_article_video" class="title"></textarea></p>'
		).animate({opacity:1.0},900,function(){});            	
	});

	/*

	edit_blog.php
	*/
	$("button#add_blog_category").click(function(event){
		event.preventDefault();

		$("div#blog_category div").append(
			'<p>Categoria: <input type="text" name="new_blog_categories[]" /></p>').animate({opacity:1.0},900,function(){});
	});			

	$("button#delete_blog_img").click(function(event){
		event.preventDefault();
		var id = $(this).attr("value");
		var img_div = $("div#blog_img_" + id);

		response = confirm("Desea realmente eliminar el elemento? ");
				//alert(id);
				if(response == true){
					$.post("edit_blog.php",{img_id:id},function(data){
						//alert(data);
					});
					
					img_div.animate({opacity:0.0,height:0},900,function(){
						$(this).remove();
					});					
		}		
	});



	$("button#delete_article").click(function(event){
		event.preventDefault();
		var id = $(this).attr("value");
		var article_div = $("div#article_" + id);

		response = confirm("Desea realmente eliminar el elemento? ");

		if(response == true){
			$.post("edit_blog.php",{del_article_id:id},function(data){
				alert(data);
			});

			article_div.animate({opacity:0.0,height:0},900,function(){
				$(this).remove();
			});					
		}		
	});


}); 