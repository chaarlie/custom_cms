<section id="bottom">
	<div class="bottom_inner wrapperoverlay">
    	<div class="widget"><h6>Social Media</h6>
            <div class="socialmedia">
                <a class="facebook" href="https://www.facebook.com/ZerosDesignSrl" target="_blank"><span>Facebook</span></a>
                <a class="twitter" href="https://twitter.com/ZerosDesignSRL" target="_blank"><span>Twitter</span></a>
                <a class="dribbble" href="index.html" target="_blank"><span>Dribbble</span></a>
                <a class="vimeo" href="index.html" target="_blank"><span>Vimeo</span></a>
                <a class="flickr" href="index.html" target="_blank"><span>Flickr</span></a>
                <a class="googleplus" href="index.html" target="_blank"><span>Google+</span></a>
            </div>
        </div>
    </div>
</section> <!-- END #bottom -->
    
<footer id="footer">
	<div class="footer_inner wrapper clearfix">
    	<div class="column one_third seperator">
        	<div class="widget">
                <h6 class="sectiontitle">Acerca de Zeros</h6>
                <div id="text-widget">
                     <?php
                     $result=$dbOp->select('','',true,' SELECT content FROM about_main_paragraphs WHERE about_main_contents_id=1 LIMIT 1',$mysqli);

                     foreach ($result as $key):
                     ?>
                        <p><?php echo nl2br($key[0]);?></p>
                     <?php
                     endforeach;
                     ?>
                    <p>
                        <a href="about.php" class="color readmore"><span class="readmoreicon">+</span>leer m&aacute;s</a>
                    </p>
                </div>
            </div>
        </div>
        
        <div class="column one_third seperator">
            
        </div>
        
        <!-- <div class="column one_third last seperator">
        	<div class="widget">
            	<h6 class="sectiontitle">&Uacute;ltimos Tweets</h6>
            		<div id="twitter-widget">
						<ul class="tweet-list">
                			<li><span class="tweet_time"><a href="">about 5 hours ago</a></span> <span class="tweet_text">Nullam id dolor id nibh ultricies vehicula ut id elit.</span></li>
                			<li><span class="tweet_time"><a href="">about 8 hours ago</a></span> <span class="tweet_text">Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</span></li>
                			<li><span class="tweet_time"><a href="">about 2 days ago</a></span> <span class="tweet_text">Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem.</span></li>
                		</ul>
                    </div>
                </div>
        </div> -->

        <div class="column one_third last seperator">
            <div class="widget">
                <h6 class="sectiontitle">Contacto</h6>
                    
                        <p>Av. Alma Máter Esq. José Contreras,o Plaza Coconut 2do. Nivel<br/>Tel: (809) 741.5766</p>
                    
            </div>
        </div> 
	</div>  <!-- END .footer_inner -->
    <div class="footer_bottom clearfix">
    	<div class="left_float">Copyright © Zeros Design. Todos los derechos reservados.</div>     
        <div class="right_float"><a href=""></a><a href=""></a></div>
    </div>
</footer> <!-- END #footer -->

<a href="" class="totop" title="Back to top">ToTop</a>

</div> <!-- END #page -->


<!-- jquery -->
<script src="resources/js/jquery.isotope.min.js"></script>
<script src="resources/js/jquery.flexslider-min.js"></script>
<script src='resources/js/jquery.easing.1.3.js'></script>
<script src='resources/js/jquery.easing.compatibility.js'></script>
<script src="resources/js/jquery.fancybox.pack.js"></script>
<script src="resources/jplayer/jquery.jplayer.min.js"></script>
<script src="resources/js/script.js"></script>
<script src="files/js/loader.js"></script>


</body>
</html>

