<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang="es"> <!--<![endif]--><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- scaling not possible (for smartphones, ipad, etc.) -->
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />

<title>Zeros Design</title>


<link href="resources/css/fonts.css" rel="stylesheet" type="text/css" />
<link href="resources/css/style.css" rel="stylesheet" type="text/css" />
<link href="resources/css/isotope.css" rel="stylesheet" type="text/css" />
<link href="resources/css/flexslider.css" rel="stylesheet" type="text/css" />
<link href="resources/css/fancybox.css" rel="stylesheet" type="text/css" />
<link href="resources/jplayer/jplayer.css" rel="stylesheet" type="text/css"  />
<link href="resources/css/mqueries.css" rel="stylesheet" type="text/css" media="screen" />

<script src="resources/js/jquery-1.7.1.min.js"></script>
<script src="resources/js/jquery.modernizr.min.js"></script>

</head>


<?php

?>